#!/bin/bash

PWD_LOCAL_RUN=$(pwd)
[[ $0 =~ "/" ]] && cd ${0%/*}
PWD_LOCAL=$(pwd)
SERVER_LIST_FILE=${PWD_LOCAL}/server_list.json

. ${PWD_LOCAL}/function.sh 

for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
do
    set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
    IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')
    if [ "${SSH_PASSWD}z" != "z" ]
    then
        MASTER_SSH_COMMAND="sshpass -p ${SSH_PASSWD} ssh -p ${SSH_PORT} -l ${SSH_USER} ${IP_ADDR}"
        MASTER_SCP_COMMAND=("sshpass -p ${SSH_PASSWD} scp -P ${SSH_PORT}" "${SSH_USER}@${IP_ADDR}")
        break
    else
        MASTER_SSH_COMMAND="ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR}"
        MASTER_SCP_COMMAND=("scp -P ${SSH_PORT} -i ${SSH_KEY_FILE}" "${SSH_USER}@${IP_ADDR}")
        break
    fi
done

if ${MASTER_SSH_COMMAND} 'kubectl get cm -n alauda-system global-var -o json' >/tmp/global-var.json
then
    log_print upgrade success 'get global-var configmap success'
else
    log_print upgrade error 'get global-var configmap error'
    exit 1
fi
if jq . /tmp/global-var.json >/dev/null 2>&1
then
    log_print upgrade success '/tmp/global-var.json file format is correct'
else
    log_print upgrade error '/tmp/global-var.json file format not correct'
    exit 1
fi

REDIS_NODE_LIST=($(jq .data /tmp/global-var.json | awk '/^  "redis.trib.para":/{for(i=2;i<=NF;++i) printf $i "\t";printf "\n"}' | sed -e 's/"//g' -e 's/,//g' -e 's/:263../\n/g' | sed -e 's/^[[:space:]]*//g' -e '/^$/d' | sort | uniq))
[ ${#REDIS_NODE_LIST[@]} == 3 ] && redis_mode=old || redis_mode=new
vaule_temp=$(${MASTER_SSH_COMMAND} 'kubectl get deploy -n alauda-system lanaya -o yaml' | grep -A1 '\- name: ELASTICSEARCH_URL' | awk '/value:/{print $2}' | sed -e 's/@.*$//' -e 's#http://##')
es_pass=${vaule_temp#*:}
es_user=${vaule_temp%:*}
es_pass_64=$(echo ${es_pass} | base64)
es_user_64=$(echo ${es_user} | base64)
redis_pass=$(jq .data /tmp/global-var.json | awk '/^  "redis.passwd":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
redis_pass_64=$(jq .data /tmp/global-var.json | awk '/^  "redis.passwd.base64":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
DB_USER=$(jq .data /tmp/global-var.json | awk '/^  "db.user":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
DB_PASSWORD=$(jq .data /tmp/global-var.json | awk '/^  "db.password":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
DB_USER_64=$(echo ${DB_USER} | base64)
DB_PASSWORD_64=$(echo ${DB_PASSWORD} | base64)
DB_ENGINE=$(jq .data /tmp/global-var.json | awk '/^  "db.engine":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
DB_HOST=$(jq .data /tmp/global-var.json | awk '/^  "db.host":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
DB_PORT=$(jq .data /tmp/global-var.json | awk '/^  "db.port":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
LOG_NODE_LIST=($(jq .data /tmp/global-var.json | awk '/^  "es.list":/{print $2}' | sed -e 's/"//g' -e 's/,/ /g'))
DOMAIN_NAME=$(jq .data /tmp/global-var.json | awk '/^  "global.vip":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
CHART_ENDPOINT=$(jq .data /tmp/global-var.json | awk '/^  "ake.chart.repo.url":/{print $2}' | sed -e 's/"//g' -e 's/,//g')
REGISTRY_ENDPOINT=$(jq .data /tmp/global-var.json | awk '/^  "registry.endpoint":/{print $2}' | sed -e 's/"//g' -e 's/,//g')


## 生成redis.toml 文件
    cat <<EOF >/tmp/redis.toml
# db 密码
DB_PASSWORD="${DB_PASSWORD}"

# jakrio 密码
JAKIRO_PASSWORD='TVFcc]J%p35x/Z>'

# normal为单例，cluster为集群
REDIS_TYPE_READER="cluster"

# redis的DB name，只有在单例模式下才管用，集群模式会被忽略
REDIS_DB_NAME_READER=0

# 密码,如果没有密码则传空
REDIS_DB_PASSWORD_READER="${redis_pass}"
REDIS_DB_PASSWORD_WRITER="${redis_pass}"

# 最大连接数, 默认为32
REDIS_MAX_CONNECTIONS_READER=32

# redis key的前缀
REDIS_KEY_PREFIX_READER="alauda"

# 当用户屏蔽了CONFIG命令时,需要把这个值改为true，只有集群模式有这个变量
REDIS_SKIP_FULL_COVER_CHECK_READER=false

# redis连接和操作的超时时间为5秒
REDIS_TIMEOUT_READER=5
EOF
if [ "${redis_mode}z" == "oldz" ]
then
    echo "# ip，集群模式下IP会有多个，以逗号分割，IP数量和端口数量一致" >> /tmp/redis.toml
    echo 'REDIS_HOST_READER=["'${REDIS_NODE_LIST[0]}'","'${REDIS_NODE_LIST[1]}'","'${REDIS_NODE_LIST[2]}'","'${REDIS_NODE_LIST[0]}'","'${REDIS_NODE_LIST[1]}'","'${REDIS_NODE_LIST[2]}'"]' >> /tmp/redis.toml
    echo '# 端口，集群模式下端口会有多个，以逗号分割，IP数量和端口数量一致' >> /tmp/redis.toml
    echo 'REDIS_PORT_READER=["26379","26379","26379","26380","26380","26380"]' >> /tmp/redis.toml
else
    echo "# ip，集群模式下IP会有多个，以逗号分割，IP数量和端口数量一致" >> /tmp/redis.toml
    echo 'REDIS_HOST_READER=["'${REDIS_NODE_LIST[0]}'","'${REDIS_NODE_LIST[1]}'","'${REDIS_NODE_LIST[2]}'","'${REDIS_NODE_LIST[3]}'","'${REDIS_NODE_LIST[4]}'","'${REDIS_NODE_LIST[5]}'"]' >> /tmp/redis.toml
    echo '# 端口，集群模式下端口会有多个，以逗号分割，IP数量和端口数量一致' >> /tmp/redis.toml
    echo 'REDIS_PORT_READER=["26379","26379","26379","26379","26379","26379"]' >> /tmp/redis.toml
fi
cat <<EOF >/tmp/helm_install.sh
helm repo update ; sleep 5
helm upgrade --set global.variable.redis_toml='$(cat /tmp/redis.toml | base64 -w 0)' \\
             --set global.db_user_64='${DB_USER_64}' \\
             --set global.db_passwd_64='${DB_PASSWORD_64}' \\
             --set global.es_user_64='${es_user_64}' \\
             --set global.es_passwd_64='${es_pass_64}' \\
             --set global.es_passwd='${es_pass}' \\
             --set global.variable.redis_passwd='${redis_pass}' \\
             --set global.variable.redis_passwd_base64='${redis_pass_64}' \\
             --set global.variable.db_engine='${DB_ENGINE}' \\
             --set global.variable.db_host='${DB_HOST}' \\
             --set global.variable.db_port='${DB_PORT}' \\
             --set global.variable.db_user='${DB_USER}' \\
             --set global.variable.db_password='${DB_PASSWORD}' \\
             --set global.variable.es_node1='${LOG_NODE_LIST[0]}' \\
             --set global.variable.es_node2='${LOG_NODE_LIST[1]}' \\
             --set global.variable.es_node3='${LOG_NODE_LIST[2]}' \\
             --set global.variable.redis_node1='${REDIS_NODE_LIST[0]}' \\
             --set global.variable.redis_node2='${REDIS_NODE_LIST[1]}' \\
             --set global.variable.redis_node3='${REDIS_NODE_LIST[2]}' \\
             --set global.variable.redis_node4='${REDIS_NODE_LIST[3]}' \\
             --set global.variable.redis_node5='${REDIS_NODE_LIST[4]}' \\
             --set global.variable.redis_node6='${REDIS_NODE_LIST[5]}' \\
             --set global.variable.redis_node_num='${#REDIS_NODE_LIST[@]}' \\
             --set global.variable.redis_mode='${redis_mode}' \\
             --set global.variable.vip='${DOMAIN_NAME}' \\
             --set global.variable.chart_repo_url='${CHART_ENDPOINT}' \\
             --set global.variable.regrstry_ep='${REGISTRY_ENDPOINT}' \\
             --set global.registry='${REGISTRY_ENDPOINT}' \\
         alauda-ace stable/ACE 
EOF
${MASTER_SCP_COMMAND[0]} /tmp/helm_install.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
if ${MASTER_SSH_COMMAND} 'bash /tmp/helm_install.sh'
then
    log_print upgrade info 'upgrade success'
else
    log_print upgrade error 'upgrade error'
fi
## migrate
#log_print upgrade info 'sleep 120'
#sleep 120
log_print upgrade info "Execute the /tmp/migrate.sh script, Command is as follows: ${MASTER_SSH_COMMAND} '  bash /tmp/migrate.sh'"
cat ${PWD_LOCAL}/migrate.sh | grep -v '^lightkeeper2 sh /lightkeeper/scripts/create_weblab_json.sh' >/tmp/migrate.sh
${MASTER_SCP_COMMAND[0]} /tmp/migrate.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
${MASTER_SCP_COMMAND[0]} ${PWD_LOCAL}/function.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
log_print upgrade info "Make sure that all pods are working properly, then execute ${MASTER_SSH_COMMAND} 'bash /tmp/migrate.sh'"

#if ${MASTER_SSH_COMMAND} '  bash /tmp/migrate.sh'
#then
#    log_print upgrade info 'migrate success'
#else
#    log_print upgrade error 'migrate error, deployment failed'
#    exit 1
#fi
#log_print upgrade warning 'After the script is executed, it is not sure that all components migrate are all successful'
