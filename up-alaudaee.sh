#!/bin/bash
########################################################
#
#  up ACE 
#  version: v0.1.0
#  update: 2018-9-7
#  writer: WangHongtao
#
########################################################

set -e
## 设置变量
ALAUDA_EE_TEMP=/tmp/alauda_ee ; rm -rf ${ALAUDA_EE_TEMP} ; mkdir ${ALAUDA_EE_TEMP}
PWD_LOCAL_RUN=$(pwd)
[[ $0 =~ "/" ]] && cd ${0%/*}
PWD_LOCAL=$(pwd)
CHART_TGZ_FILE=$(ls ${PWD_LOCAL}/ACE*.tgz | head -n 1)
DOCKER_PATH=/var/lib/docker
NET_DEV=eth0
ACP_PATH=ACP
SERVER_LIST_FILE=${PWD_LOCAL}/server_list.json
HAPROXY_FILE=haproxy.cfg
cp ${PWD_LOCAL}/server_check.sh ${ALAUDA_EE_TEMP} ; SERVER_CHECK_SH_FILE=${ALAUDA_EE_TEMP}/server_check.sh
cp ${PWD_LOCAL}/function.sh ${ALAUDA_EE_TEMP} ; FUNCTION_SH_FILE=${ALAUDA_EE_TEMP}/function.sh
check_type='init'
. ${PWD_LOCAL}/function.sh "$@"
cd ${PWD_LOCAL}/${ACP_PATH} ; ACP_PWD_LOCAL=$(pwd) ; cd $PWD_LOCAL_RUN
ACP_INIT_SCRIPT=${ACP_PWD_LOCAL}/init.sh

file_check()
{
    ## 判断 server_list 文件是否存在及文件格式是否合法
    check_type='check_file'
    if [ -f ${SERVER_LIST_FILE} ]
    then
        if jq '.' ${SERVER_LIST_FILE} >/dev/null 2>&1
        then
            RETURN_LEVEL='info'
            RETURN_MESSAGE="${SERVER_LIST_FILE} file format is correct"
        else
            RETURN_LEVEL='error'
            RETURN_MESSAGE="${SERVER_LIST_FILE} file format is incorrect"
        fi
    else
        RETURN_LEVEL='error'
        RETURN_MESSAGE="${SERVER_LIST_FILE} file does not exist"
    fi
    log_print ${check_type} ${RETURN_LEVEL} "${RETURN_MESSAGE}"
    if [ ${RETURN_LEVEL} == error ] ; then exit 1 ; fi
}

server_check()
{

## 检查Server_list 文件中的服务器能否登录，如果用户名非 root ，检查是否具备免密   权限，检查操作系统配置是否符合要求
## 从 server_list.json 中提取服务器角色，并检查master 数量是否合法
## 检查传递进来的参数是否合法
    check_node_env=0
    if [ "${NETWORK_CIDR}" == '10.96.0.0/16' ]
    then
        log_print ${check_type} error 'The cidr of the k8s cluster cannot use 10.96.0.0/16, default is 10.199.0.0/16' ; check_node_env=1
    fi
    MASTERS='' ; NODES='' ; MASTER_NODE_LIST='' ; GLOBAL_NODE_LIST='' ; REDIS_NODE_LIST='' ; LOG_NODE_LIST=''
    MASTER_SSH_COMMAND='' ; MASTER_SCP_COMMAND=''
    for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
    do
        check_type='node_os_config'
## 遍历 server_list.json 文件中配置的服务器信息
        set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
        IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')
        [ $(jq ".[${i}].server_role.master" ${SERVER_LIST_FILE}) == true ] && MASTER_NODE_LIST="${MASTER_NODE_LIST} ${IP_ADDR}"
        [ $(jq ".[${i}].server_role.global" ${SERVER_LIST_FILE}) == true ] && GLOBAL_NODE_LIST="${GLOBAL_NODE_LIST} ${IP_ADDR}"
        [ $(jq ".[${i}].server_role.redis" ${SERVER_LIST_FILE}) == true ] && REDIS_NODE_LIST="${REDIS_NODE_LIST} ${IP_ADDR}"
        [ $(jq ".[${i}].server_role.log" ${SERVER_LIST_FILE}) == true ] && LOG_NODE_LIST="${LOG_NODE_LIST} ${IP_ADDR}"
## 生成 ake 参数
        if [ "${SSH_PASSWD}z" != "z" ]
        then
            [ $(jq ".[${i}].server_role.master" ${SERVER_LIST_FILE}) == true ] && MASTERS="${MASTERS};${IP_ADDR}(user=${SSH_USER},ssh_pass=${SSH_PASSWD},port=${SSH_PORT})" || NODES="${NODES};${IP_ADDR}(user=${SSH_USER},ssh_pass=${SSH_PASSWD},port=${SSH_PORT})"
## 判断能否 ssh 登录，如果可以，检查这台服务器
            if timeout --signal=9 5 sshpass -p "${SSH_PASSWD}" scp -o StrictHostKeyChecking=no -P ${SSH_PORT} ${ALAUDA_EE_TEMP}/* ${SSH_USER}@${IP_ADDR}:/tmp/ >/dev/null 2>&1
            then
                SSH_LOGIN_RETURN_LEVEL=success
                if [ ${SSH_USER} == root ]
                then
                    sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} bash /tmp/${SERVER_CHECK_SH_FILE##*/} dates=$(date "+%s") date_hms=$(date "+%T") net_dev=${NET_DEV} DOCKER_PATH=${DOCKER_PATH} auto_check=true 2>/dev/null || check_node_env=1
                else
                    if timeout --signal=9 5 sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR}   ls / >/dev/null 2>&1
                    then
                        sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR}   bash /tmp/${SERVER_CHECK_SH_FILE##*/} dates=$(date "+%s") date_hms=$(date "+%T") net_dev=${NET_DEV} DOCKER_PATH=${DOCKER_PATH} auto_check=true NOT_DEPLOY_K8S="${NOT_DEPLOY_K8S}" 2>/dev/null || check_node_env=1
                    else
                        log_print ${check_type} error "User ${SSH_USER} does not have   permission on server ${IP_ADDR}"
                        check_node_env=1
                    fi
                fi
            else
                SSH_LOGIN_RETURN_LEVEL=error ; check_node_env=1
            fi
        else
            [ $(jq ".[${i}].server_role.master" ${SERVER_LIST_FILE}) == true ] && MASTERS="${MASTERS};${IP_ADDR}(user=${SSH_USER},ssh_private_key_file=${SSH_KEY_FILE},port=${SSH_PORT})" || NODES="${NODES};${IP_ADDR}(user=${SSH_USER},ssh_private_key_file=${SSH_KEY_FILE},port=${SSH_PORT})"
            if timeout --signal=9 5 scp -o StrictHostKeyChecking=no -P ${SSH_PORT} -i ${SSH_KEY_FILE} ${ALAUDA_EE_TEMP}/* ${SSH_USER}@${IP_ADDR}:/tmp/ >/dev/null 2>&1
            then
                SSH_LOGIN_RETURN_LEVEL=success
                if [ ${SSH_USER} == root ]
                then
                    ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR} bash /tmp/${SERVER_CHECK_SH_FILE##*/} dates=$(date "+%s") date_hms=$(date "+%T") net_dev=${NET_DEV} DOCKER_PATH=${DOCKER_PATH} auto_check=true NOT_DEPLOY_K8S="${NOT_DEPLOY_K8S}"  2>/dev/null || check_node_env=1
                else
                    if timeout --signal=9 5 ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR}   ls / >/dev/null 2>&1
                    then
                        ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR}   bash /tmp/${SERVER_CHECK_SH_FILE##*/} dates=$(date "+%s") date_hms=$(date "+%T") net_dev=${NET_DEV} DOCKER_PATH=${DOCKER_PATH} auto_check=true 2>/dev/null || check_node_env=1
                    else
                        log_print ${check_type} error "User ${SSH_USER} does not have   permission on server ${IP_ADDR}"
                        check_node_env=1
                    fi
                fi
            else
                SSH_LOGIN_RETURN_LEVEL=error ; check_node_env=1
            fi
        fi
        RETURN_MESSAGE="ssh login ${IP_ADDR} ${SSH_LOGIN_RETURN_LEVEL}"
        check_type='check_login'
        log_print ${check_type} ${SSH_LOGIN_RETURN_LEVEL} "${RETURN_MESSAGE}"
    done
## 选择第一个 master 作为以后 ssh 登录操作的对象
    for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
    do
        set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
        IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')
        if [ "${SSH_PASSWD}z" != "z" ]
        then
            MASTER_SSH_COMMAND="sshpass -p ${SSH_PASSWD} ssh -p ${SSH_PORT} -l ${SSH_USER} ${IP_ADDR}"
            MASTER_SCP_COMMAND=("sshpass -p ${SSH_PASSWD} scp -P ${SSH_PORT}" "${SSH_USER}@${IP_ADDR}")
            break
        else
            MASTER_SSH_COMMAND="ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR}"
            MASTER_SCP_COMMAND=("scp -P ${SSH_PORT} -i ${SSH_KEY_FILE}" "${SSH_USER}@${IP_ADDR}")
            break
        fi
    done
    MASTERS="${MASTERS#;}" ETCDS="${MASTERS}"
    [ "${MASTER_IS_NODE}z" == "truez" ] && NODES="${MASTERS}${NODES}" || NODES="${NODES#;}"
    REDIS_NODE_LIST=(${REDIS_NODE_LIST[@]}) ; LOG_NODE_LIST=(${LOG_NODE_LIST[@]}) ; GLOBAL_NODE_LIST=(${GLOBAL_NODE_LIST[@]}) ; MASTER_NODE_LIST=(${MASTER_NODE_LIST[@]})
    check_type='k8s_node_role'
    check_role_env=0
## 判断 server_list.json 中配置的服务器，给定的redis、master、log 等角色的数量是否合法
    if [ ${#REDIS_NODE_LIST[@]} == 3 ] || [ ${#REDIS_NODE_LIST[@]} -ge 6 ]
    then
        LOGIN_RETURN_LEVEL=success ; RETURN_MESSAGE="The number of nodes running the redis cluster is ${#REDIS_NODE_LIST[@]}"
    else
        LOGIN_RETURN_LEVEL=error ; RETURN_MESSAGE='The number of nodes running the redis cluster is not 3 or 6' ; check_role_env=1
    fi
    log_print ${check_type} ${LOGIN_RETURN_LEVEL} "${RETURN_MESSAGE}"
    if [ ${#LOG_NODE_LIST[@]} == 3 ]
    then
        LOGIN_RETURN_LEVEL=success ; RETURN_MESSAGE="The number of nodes running the log cluster is 3"
    else
        LOGIN_RETURN_LEVEL=error ; RETURN_MESSAGE='The number of nodes running the log cluster is not 3' ; check_role_env=1
    fi
    log_print ${check_type} ${LOGIN_RETURN_LEVEL} "${RETURN_MESSAGE}"
    if [ ${#GLOBAL_NODE_LIST[@]} -ge 1 ]
    then
        LOGIN_RETURN_LEVEL=success ; RETURN_MESSAGE="The number of nodes running global component is ${#GLOBAL_NODE_LIST[@]}"
    else
        LOGIN_RETURN_LEVEL=error ; RETURN_MESSAGE='Node not running global component not found' ; check_role_env=1
    fi
    log_print ${check_type} ${LOGIN_RETURN_LEVEL} "${RETURN_MESSAGE}"
    if [ ${#MASTER_NODE_LIST[@]} == 1 ] || [ ${#MASTER_NODE_LIST[@]} == 3 ]
    then
        LOGIN_RETURN_LEVEL=success ; RETURN_MESSAGE="The number of master nodes in the kubernet cluster is ${#MASTER_NODE_LIST[@]}"
    else
        LOGIN_RETURN_LEVEL=error ; RETURN_MESSAGE='The number of master nodes in a kubernet cluster can only be 1 or 3.' ; check_role_env=1
    fi
    log_print ${check_type} ${LOGIN_RETURN_LEVEL} "${RETURN_MESSAGE}"
    if [ ${#MASTER_NODE_LIST[@]} == 3 ] && [ "${K8S_CONTROL_LB}z" == 'nonez' ] && [ "${MAKE_LB}z" == 'nonez' ]
    then
        log_print ${check_type} error "The number of masters is 3, so you must specify the k8s api server lb with the --kube_controlplane_endpoint parameter."
        check_role_env=1
    fi
## 检查完毕，检查结果
    if [ "${check_node_env}" == "1" ]
    then
        log_print ${check_type} error 'Some server checks failed' ;  exit 1
    else
        log_print ${check_type} info 'All servers check through'
    fi
    if [ "${check_role_env}" == "1" ]
    then
        log_print ${check_type} error 'Some roles have incorrect number of nodes' ; exit 1
    else
        log_print ${check_type} info 'The roles of all nodes of the ACE platform are configured correctly.'
    fi
}

init_base()
{
## 调用 ACP 的 init.sh 脚本，初始化基本环境，部署镜像仓库、软件源、chart repo，将 ACE 的 chart 传到 chart repo 上
    . ${ACP_INIT_SCRIPT}
    sleep 5
    if curl -u $(echo ${CHART_ENDPOINT} | sed -e 's/@.*$//g' -e 's#^.*://##g') --data-binary "@${CHART_TGZ_FILE}" $(echo ${CHART_ENDPOINT} | sed 's#^http.*@#http://#')api/charts 2>/dev/null | jq . | grep -q -E '"saved": true|"file already exists"' 2>/dev/null 
    then
        log_print ${check_type} success "upload ACE chart to ${CHART_ENDPOINT} success"
    else
        log_print ${check_type} error "upload ACE chart to ${CHART_ENDPOINT} failed" ; exit 1
    fi
## 如果需要，启动数据库（仅限 poc 环境）
    if [ "${MAKE_DB}z" == "truez" ]
    then
        log_print deploy_db info 'Deploy database'
        log_print deploy_db warning 'Used only for poc testing !!!!!!'
        if [ "${USE_MYSQL}z" == "truez" ]
        then
            cat <<EOF >/alauda/.run_db.sh
#!/bin/bash
docker run -d --restart=always --name mysql -p 3306:3306 -v /alauda/data/mysql/db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD="sgOnxrI9WN638Hj8" ${REGISTRY_ENDPOINT}/alaudaorg/mysql-master:latest >/dev/null 2>&1
EOF
            DB_PORT='3306' ; DB_PASSWORD='sgOnxrI9WN638Hj8' ; DB_USER='root' ; DB_HOST="${LOCAL_IP}" ; DB_ENGINE='mysql'
        else
            cat <<EOF >/alauda/.run_db.sh
#!/bin/bash
docker run -d --restart=always --name postgres -p 5432:5432 -v /alauda/postgresql/data:/var/lib/postgresql/data -e POSTGRES_PASSWORD="sgOnxrI9WN638Hj8" -e POSTGRES_USER="alauda" ${REGISTRY_ENDPOINT}/alaudaorg/postgres:max >/dev/null 2>&1
EOF
            DB_PORT='5432' ; DB_PASSWORD='sgOnxrI9WN638Hj8' ; DB_USER='alauda' ; DB_HOST="${LOCAL_IP}" ; DB_ENGINE='postgresql'
        fi
        if bash /alauda/.run_db.sh
        then
            log_print deploy_db success 'Deploy sql database success'
            log_print deploy_db info "The database information: DB_PORT=$DB_PORT DB_PASSWORD=$DB_PASSWORD DB_USER=$DB_USER DB_HOST=$DB_HOST DB_ENGINE=$DB_ENGINE"
        else
            log_print deploy_db error 'Failed to start database'
            exit 1
        fi
    fi
## 检查数据库链接和写权限
    log_print check_db info 'Check the database'
    sleep 50
    /usr/bin/checkdb -e ${DB_ENGINE} -h ${DB_HOST} -k ${DB_PASSWORD} -p ${DB_PORT} -u ${DB_USER} >/tmp/check_db_info 2>&1 
    if grep -q 'Succese connect to database.' /tmp/check_db_info
    then
        log_print check_db success 'Successful access to the database'
    else
        log_print check_db error 'Access to the database failed'
        exit 1
    fi 
    if grep -q 'Success write to database.' /tmp/check_db_info
    then
        log_print check_db success "Write database successfully, user ${DB_USER} has the correct permissions"
    else
        log_print check_db error "Failed to write database, user ${DB_USER} does not have write permission"
        exit 1
    fi
## 创建数据库
    rm -rf /tmp/alaudaee_chart
    mkdir /tmp/alaudaee_chart
    tar zxf ${CHART_TGZ_FILE} -C /tmp/alaudaee_chart
    DB_NAME_LIST=($(find /tmp/alaudaee_chart -name '*.yaml' -exec grep -A 1 ' name: DB_NAME' {} \; | awk '/value:/ {print $2}'|tr -d "'" | sort))
    log_print create_db info "Start creating the following databases: ${DB_NAME_LIST[@]}"
    for i in ${DB_NAME_LIST[@]}
    do
        /usr/bin/checkdb -e ${DB_ENGINE} -h ${DB_HOST} -k ${DB_PASSWORD} -p ${DB_PORT} -u ${DB_USER} -d $i >/tmp/check_db_info 2>&1
        if grep -q -E "Success create database:  $i|pq: database \"$i\" already exists" /tmp/check_db_info
        then
            log_print create_db success "Create database $i successfully"
        else
            log_print create_db error "Create database $i failed"
            exit 1
        fi
    done

}

install_k8s()
{

##  如果有需要，创建测试环境的 haproxy，为 k8s api server 使用
    if [ "${MAKE_LB}z" == "truez" ]
    then
        K8S_CONTROL_LB=${LOCAL_IP}
        DOMAIN_NAME=${LOCAL_IP}
        log_print install_k8s info 'Create a test lb'
        log_print install_k8s warning 'Used only for poc testing !!!!!!'
        cat ${PWD_LOCAL}/${HAPROXY_FILE} >/alauda/${HAPROXY_FILE}
        for j in 443 6443 32001 32002 32003 32004 32005 32006 32007
        do
            cat <<EOF >>/alauda/${HAPROXY_FILE}


frontend alauda_frontend_${j}
  bind *:${j}
  mode tcp
  default_backend alauda_${j}

EOF
        done
        for j in 443 6443 32001 32002 32003 32004 32005 32006 32007
        do
            cat <<EOF >>/alauda/${HAPROXY_FILE}


backend alauda_${j}
  mode tcp
  balance roundrobin
  stick-table type ip size 200k expire 30m
  stick on src
EOF
            for ((i = 0 ; i < ${#MASTER_NODE_LIST[@]} ; i++))
            do
                echo "server s${i} ${MASTER_NODE_LIST[$i]}:${j} check port ${j} inter 1000 maxconn 51200" >>/alauda/${HAPROXY_FILE}
            done
        done
        log_print install_k8s info 'Start the haproxy container'
        cat <<EOF >/alauda/.run_haproxy.sh
#!/bin/bash
docker run -d --restart=always --name controller_haproxy -v /alauda/${HAPROXY_FILE}:/etc/haproxy/haproxy.cfg --network=host ${REGISTRY_ENDPOINT}/claas/controller_haproxy:v1.1 >/dev/null 2>&1
EOF
        if bash /alauda/.run_haproxy.sh
        then
            log_print install_k8s success 'Start the haproxy container success'
        else
            log_print install_k8s error 'start the haproxy container failure' ; exit 1
        fi
    fi
    if [ "${K8S_CONTROL_LB}z" == "z" ]
    then
        log_print install_k8s error 'There can only be one parameter --make-lb and --kube_controlplane_endpoint' ; exit 1
    fi
    log_print install_k8s info "the lb addr of k8s api serveri is ${K8S_CONTROL_LB}"
## 检查 api server 的 lb
    if [ "${NOT_CHECK_LB}z" == "truez" ]
    then
        log_print install_k8s info 'do not check k8s api lb'
    elif [ "${MAKE_LB}z" == "falsez" ]
    then
        log_print install_k8s info "The address of lb is ${K8S_CONTROL_LB}, which is the same as the address of master, so don't test lb"
    else
        log_print install_k8s info 'check k8s api lb'
        cat <<EOF >/tmp/check_6443.sh
hostname -i >/tmp/check_lb.txt
cd /tmp
python -m SimpleHTTPServer 6443 >/dev/null 2>&1 &
sleep 13
kill -9 \$(ps aux | grep 'python -m SimpleHTTPServer 6443' | grep -v grep | awk '{print \$2}')
EOF
        cat <<EOF >/tmp/kill_6443.sh
kill -9 \$(ps aux | grep 'python -m SimpleHTTPServer 6443' | grep -v grep | awk '{print \$2}')
EOF
## 在三个 master 上起 http 服务，侦听6443 端口。然后 curl lb 的6443 ，看看是否转发成功。
        for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
        do
            if [ $(jq ".[${i}].server_role.master" ${SERVER_LIST_FILE}) == true ]
            then
                set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
                IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')
                TEMP_MASTER_SCP_COMMAND='' ; TEMP_MASTER_SSH_COMMAND=''
                if [ "${SSH_PASSWD}z" != "z" ]
                then
                    TEMP_MASTER_SSH_COMMAND="sshpass -p ${SSH_PASSWD} ssh -p ${SSH_PORT} -l ${SSH_USER} ${IP_ADDR}"
                    TEMP_MASTER_SCP_COMMAND=("sshpass -p ${SSH_PASSWD} scp -P ${SSH_PORT}" "${SSH_USER}@${IP_ADDR}")
                else
                    TEMP_MASTER_SSH_COMMAND="ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR}"
                    TEMP_MASTER_SCP_COMMAND=("scp -P ${SSH_PORT} -i ${SSH_KEY_FILE}" "${SSH_USER}@${IP_ADDR}")
                fi
                ${TEMP_MASTER_SCP_COMMAND[0]} /tmp/check_6443.sh ${TEMP_MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
                ${TEMP_MASTER_SSH_COMMAND} '  nohup bash /tmp/check_6443.sh >/dev/null 2>&1' &
                log_print install_k8s info "Check if forwarding from ${K8S_CONTROL_LB}:6443 to ${IP_ADDR}:6443 is normal"
                sleep 5
                check_lb=0
                for check_lb in {1..10}
                do
                    sleep 1
                    if curl ${K8S_CONTROL_LB}:6443/check_lb.txt 2>/dev/null | grep -q ${IP_ADDR}
                    then
                        log_print check_lb success "${check_lb}nd check k8s api lb success, forward to ${IP_ADDR}"
                        break
                    else
                        log_print check_lb warning "${check_lb}nd check ${K8S_CONTROL_LB}:6443 to ${IP_ADDR}:6443 failed, Will try again $((10-check_lb)) times"
                    fi
                done
                if [ ${check_lb} -gt 9 ]
                then
                    log_print install_k8s error "After 10 attempts, the forwarding through ${K8S_CONTROL_LB}:6443 to lb to ${IP_ADDR}:6443 still failed."
                    exit 1
                fi
                ${TEMP_MASTER_SCP_COMMAND[0]} /tmp/kill_6443.sh ${TEMP_MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
                ${TEMP_MASTER_SSH_COMMAND} '  bash /tmp/kill_6443.sh >/dev/null 2>&1'
            fi
        done
    fi

## 部署 k8s 集群
    cat <<EOF >/alauda/.run_up_k8s.sh
#!/bin/bash
set -e
ake deploy \\
    --dockercfg="${DOCKER_CONFIG_FILE_TEMP}" \\
    --pkg-repo="${KALDR_ENDPOINT}" \\
    --registry="${REGISTRY_ENDPOINT}" \\
    --chart_stable_repo_addr="${CHART_ENDPOINT}" \\
    --alauda_chart_repo="${CHART_ENDPOINT}" \\
    --oidc_client_id="alauda-auth" \\
    --acp-host=${K8S_CONTROL_LB} \\
    --oidc-issuer-url="https://${K8S_CONTROL_LB}/dex" \\
    --masters="${MASTERS}" \\
    --etcds="${ETCDS}" \\
    --network-opt="backend_type=${NETWORK_BACKEND}" \\
    --kube-pods-subnet="${NETWORK_CIDR}" \\
    --enabled-features=helm,alauda-ingress,alauda-dashboard,alauda-portal,alauda-oidc,alauda-base,alauda-appcore \\
    --nodes="${NODES}" \\
    --oidc_ca_file=/alauda/ssl/cert.pem \\
    --oidc_ca_key=/alauda/ssl/key.pem \\
    --kube_controlplane_endpoint=${K8S_CONTROL_LB}

EOF
## 创建证书
    log_print install_k8s info 'create /alauda/ssl/cert.pem and /alauda/ssl/key.pem'
    rm -rf /alauda/ssl
    mkdir -p /alauda/ssl
    cat << EOF > /alauda/ssl/req.cnf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
IP.1=${K8S_CONTROL_LB}
EOF

    openssl genrsa -out /alauda/ssl/ca-key.pem 2048
    openssl req -x509 -new -nodes -key /alauda/ssl/ca-key.pem -days 8000 -out /alauda/ssl/ca.pem -subj "/CN=kube-ca"
    if openssl genrsa -out /alauda/ssl/key.pem 2048
    then
        log_print install_k8s success 'create /alauda/ssl/key.pem success'
    else
        log_print install_k8s success 'create /alauda/ssl/key.pem failed' ; exit 1
    fi
    openssl req -new -key /alauda/ssl/key.pem -out /alauda/ssl/csr.pem -subj "/CN=kube-ca" -config /alauda/ssl/req.cnf
    if openssl x509 -req -in /alauda/ssl/csr.pem -CA /alauda/ssl/ca.pem -CAkey /alauda/ssl/ca-key.pem -CAcreateserial -out /alauda/ssl/cert.pem -days 8000 -extensions v3_req -extfile /alauda/ssl/req.cnf
    then
        log_print install_k8s success 'create /alauda/ssl/cert.pem success'
    else
        log_print install_k8s success 'create /alauda/ssl/cert.pem failed' ; exit 1
    fi

    [ "${K8S_CONTROL_LB}z" == "z" ] && sed -i '/--kube_controlplane_endpoint.*$/d' /alauda/.run_up_k8s.sh
    [ "${NODES}z" == 'z' ] && sed -i '/--nodes=.*$/d' /alauda/.run_up_k8s.sh
    [ "${ACP_SIMPLE}z" == 'truez' ] && sed -i '/--enabled-features=.*$/d' /alauda/.run_up_k8s.sh
    [ "${DEBUG}z" == "truez" ] && sed -i 's/^ake deploy/ake deploy --debug/' /alauda/.run_up_k8s.sh
    tailf -n 0 /alauda/alauda.log 2>/dev/null 2>/dev/null &
    if bash /alauda/.run_up_k8s.sh >> /alauda/alauda.log
    then
        kill -9 $(ps aux | grep 'tailf -n 0 /alauda/alauda.log' | grep -v grep | awk '{print $2}') >/dev/null 2>&1
        if tail -n 5 /alauda/alauda.log | grep -q '❌'
        then
            log_print install_k8s error 'Failed to deploy k8s cluster' ; exit 1
        else
            log_print install_k8s info 'Successful deployment of k8s cluster'
        fi
    else
        kill -9 $(ps aux | grep 'tailf -n 0 /alauda/alauda.log' | grep -v grep | awk '{print $2}') >/dev/null 2>&1
        log_print install_k8s error 'Failed to deploy k8s cluster'
        exit 1
    fi
    if ${MASTER_SSH_COMMAND} "cat /sys/fs/cgroup/memory/kubepods/memory.kmem.slabinfo 2>&1 | grep -Eq '输入/输出错误|Input/output error'"
    then
        log_print up_alauda_ee success 'k8s version is correct, it is hexiaoxi modified'
    else
        log_print up_alauda_ee error 'K8s version error, not hexiaoxi modified'
    fi

}

check_k8s()
{
## 如果是用私有仓库，检查私有仓库地址是否加到 docker 的insecure-registries 中
    if [ "${REGISTRY_ENDPOINT}z" != 'index.alauda.cnz' ]
    then
        check_docker_registry=0
        for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
        do
            set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
            IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')
            log_print check_k8s info "Check the 'Insecure Registries' configuration of the docker for this server ${IP_ADDR}"
            if [ "${SSH_PASSWD}z" != "z" ]
            then
                if ! sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} "docker info 2>&1" | sed -e '1,/^Insecure Registries:/d' | grep -q ${REGISTRY_ENDPOINT} 2>/dev/null
                then
                    log_print check_k8s error "server ${IP_ADDR} The docker configuration of this server is incorrect. The content of the ${REGISTRY_ENDPOINT} is not in the configuration item 'Insecure Registries'"
                    check_docker_registry=1
                fi
            else
                if ! ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR} "docker info 2>&1" | sed -e '1,/^Insecure Registries:/d' | grep -q ${REGISTRY_ENDPOINT} 2>/dev/null
                then
                    log_print check_k8s error "server ${IP_ADDR} The docker configuration of this server is incorrect. The content of the ${REGISTRY_ENDPOINT} is not in the configuration item 'Insecure Registries'"
                    check_docker_registry=1
                fi
            fi
        done
        if [ "${check_docker_registry}" == 1 ]
        then
            log_print check_k8s error "Some servers have docker configuration error"
            exit 1
        fi
    fi

## 检查 k8s 集群是否正常
    cat <<EOF >/tmp/check_k8s_cluster.sh
#!/bin/bash
kubernets_cluster_status=0
\$(whereis -b kubectl | awk '{print \$2}') get pod --all-namespaces -o wide | sed 1d >/tmp/kubectl_get_pod.txt
j=1 ; >/tmp/not_ready_pod_list.txt
for i in \$(cat /tmp/kubectl_get_pod.txt | awk '{print \$3}')
do
    pod_ready=(\${i//// })
    [ "\${pod_ready[0]}" != "\${pod_ready[1]}" ] && echo \$j >>/tmp/not_ready_pod_list.txt
    ((j++))
done
if [ -s /tmp/not_ready_pod_list.txt ]
then
    sed_parameter='' ; kubernets_cluster_status=1
    for i in \$(cat /tmp/not_ready_pod_list.txt)
    do
        sed_parameter="\${sed_parameter}p;\$i"
    done
    echo -e "\033[1;34m[check_k8s     ]\033[1;31m[ERROR   ]\033[0m[\$(date '+%Y%m%d-%T')][\$(hostname -i | head -n 1)]\t\033[1;31m[The following pods are not working properly]\033[0m"
    cat /tmp/kubectl_get_pod.txt | sed -n "\${sed_parameter/p;/}p"
fi
\$(whereis -b kubectl | awk '{print \$2}') get no | sed 1d >/tmp/kubectl_get_no.txt
j=1 ; >/tmp/not_ready_node_list.txt
for i in \$(cat /tmp/kubectl_get_no.txt | awk '{print \$2}')
do
    [ \$i == 'Ready' ] || echo \$j >>/tmp/not_ready_node_list.txt
    ((j++))
done
if [ -s /tmp/not_ready_node_list.txt ]
then
    sed_parameter='' ; kubernets_cluster_status=1
    for i in \$(cat /tmp/not_ready_node_list.txt)
    do
        sed_parameter="\${sed_parameter}p;\$i"
    done
    echo -e "\033[1;34m[check_k8s     ]\033[1;31m[ERROR   ]\033[0m[\$(date '+%Y%m%d-%T')][\$(hostname -i | head -n 1)]\t\033[1;31m[The following nodes are not working properly]\033[0m"
    cat /tmp/kubectl_get_no.txt | sed -n "\${sed_parameter/p;/}p"
fi
if [ \${kubernets_cluster_status} == 1 ]
then
    echo -e "\033[1;34m[check_k8s     ]\033[1;31m[ERROR   ]\033[0m[\$(date '+%Y%m%d-%T')][\$(hostname -i | head -n 1)]\t\033[1;31m[The k8s cluster found an error and cannot continue]\033[0m"
    exit 1
fi
EOF
## 检查集群
    ${MASTER_SCP_COMMAND[0]} /tmp/check_k8s_cluster.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
    for i in `seq 1 6`
    do
        log_print check_k8s info 'info Wait 20 seconds' ; sleep 20
        if ${MASTER_SSH_COMMAND} '  bash /tmp/check_k8s_cluster.sh 2>/dev/null'
        then
            log_print check_k8s success "In the k8s cluster, all pod and node states are ready."
            break
        else
            log_print check_k8s error "The k8s cluster status is abnormal"
            if [ $i -gt 6 ]
            then
                log_print check_k8s error "After three checks, the node and pod status of the k8s cluster is still not all normal. Please check" ; 
            fi
        fi
    done
## 获取 node 的 json 文件
    log_print check_k8s info 'execute "kubectl get node -o json"'
    if ${MASTER_SSH_COMMAND} '  $(whereis -b kubectl | awk "{print \$2}") get node -o json' >/tmp/kubectl_get_node.json
    then
        if ! jq . /tmp/kubectl_get_node.json >/dev/null 2>&1
        then
            log_print check_k8s error "Failed to execute ${MASTER_SSH_COMMAND} '  \$(whereis -b kubectl | awk {print \$2}) get node -o json' >/tmp/kubectl_get_node.json"
            exit 1
        fi
    else
        log_print check_k8s error "Failed to execute ${MASTER_SSH_COMMAND} '  \$(whereis -b kubectl | awk {print \$2}) get node -o json' >/tmp/kubectl_get_node.json"
        exit 1
    fi

    if [ "${check_docker_registry}" == "1" ]
    then
        log_print check_k8s error 'Some server docker configuration errors' ; exit 1
    fi

}

config_k8s()
{
## 配置 k8s 集群，给 node 加 label
## label 从/tmp/node_lalel_${IP_ADDR}.list 文件中获取
## k8s 集群的 node 的信息，从/tmp/kubectl_get_node.json 文件中获取
    echo '#!/bin/bash' >/tmp/set_node_label.sh
    echo 'set -e'>>/tmp/set_node_label.sh
    for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
    do
        IP_ADDR=$(jq ".[${i}].ip_addr" ${SERVER_LIST_FILE} | sed 's/"//g')
        for j in $(jq '.items | keys | .[]' /tmp/kubectl_get_node.json)
        do
            if [ $(jq ".items[$j].status.addresses[0].type" /tmp/kubectl_get_node.json | sed 's/"//g') == "InternalIP" ]
            then
                [ $(jq ".items[$j].status.addresses[0].address" /tmp/kubectl_get_node.json | sed 's/"//g') == ${IP_ADDR} ] && K8S_NODE_NAME=$(jq ".items[$j].status.addresses[1].address" /tmp/kubectl_get_node.json | sed 's/"//g')
            else
                [ $(jq ".items[$j].status.addresses[1].address" /tmp/kubectl_get_node.json | sed 's/"//g') == ${IP_ADDR} ] && K8S_NODE_NAME=$(jq ".items[$j].status.addresses[0].address" /tmp/kubectl_get_node.json | sed 's/"//g')
            fi
        done
        echo "\$(whereis -b kubectl | awk '{print \$2}') label nodes ${K8S_NODE_NAME} ip=${IP_ADDR} --overwrite" >>/tmp/set_node_label.sh
        for j in $(jq ".[$i].server_role | keys | .[]" ${SERVER_LIST_FILE} | sed 's/"//g')
        do
            echo "\$(whereis -b kubectl | awk '{print \$2}') label nodes ${K8S_NODE_NAME} $j="$(jq ".[$i].server_role.$j" ${SERVER_LIST_FILE})" --overwrite" >>/tmp/set_node_label.sh
        done
    done
#    cat <<EOF >/tmp/set_node_label.sh
##!/bin/bash
#set -e
#for i in \$(\$(whereis -b kubectl | awk '{print \$2}') get node | sed 1d | awk '{print \$1}')
#do
#    \$(whereis -b kubectl | awk '{print \$2}') label nodes \$i ip=\$(\$(whereis -b kubectl | awk '{print \$2}') get node \$i -o jsonpath='{.status.addresses[?(@.type=="InternalIP")].address}') --overwrite
#done
#EOF
    ${MASTER_SCP_COMMAND[0]} /tmp/set_node_label.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
    if ${MASTER_SSH_COMMAND} '  bash /tmp/set_node_label.sh >/dev/null 2>&1'
    then
        log_print config_k8s success "Successful execution of /tmp/set_node_label.sh script,The commands executed are as follows: ${MASTER_SSH_COMMAND} '  bash /tmp/set_node_label.sh 2>/dev/null'"
    else
        log_print config_k8s error "Failed to execute /tmp/set_node_label.sh script,The commands executed are as follows: ${MASTER_SSH_COMMAND} '  bash /tmp/set_node_label.sh 2>/dev/null'"
        exit 1
    fi

}

up_alauda_ee()
{
## 通过 chart 启动 ACE
## 包括 pv、pvc、configmap、namespaces、limitranges、deploy

## 创建 namespaces
    log_print up_alauda_ee info 'create ACE namespaces'
    if ${MASTER_SSH_COMMAND} '  $(whereis -b kubectl | awk "{print \$2}") get namespaces | grep -q "^alauda-system" 2>/dev/null'
    then
        log_print up_alauda_ee info 'namespaces alauda-system already exists'
    else
        if ${MASTER_SSH_COMMAND} '  $(whereis -b kubectl | awk "{print \$2}") create namespace alauda-system >/dev/null 2>/dev/null'
        then
            log_print up_alauda_ee success "create ACE namespaces successfully"
        else
            log_print up_alauda_ee error "create ACE namespaces failed, The commands executed are as follows: ${MASTER_SSH_COMMAND} '  kubectl create namespace alauda-system 2>/dev/null'"
            exit 1
        fi
    fi
## 创建 crd
  #  log_print up_alauda_ee info 'create application crd'
  #  ${MASTER_SCP_COMMAND[0]} ${PWD_LOCAL}/crd.yaml ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
  #  if ${MASTER_SSH_COMMAND} '  $(whereis -b kubectl | awk "{print \$2}") create -f /tmp/crd.yaml >/dev/null 2>/dev/null'
  #  then
  #      log_print up_alauda_ee success "create application crd successfully"
  #  else
  #      log_print up_alauda_ee error "create application crd failed (请在master-1上执行 kubectl create -f /tmp/crd.yaml )"
  #  fi

  # sleep 3
## 通过 helm 安装 ACE
    log_print up_alauda_ee info 'Install ACE via helm'
    [ ${#REDIS_NODE_LIST[@]} == 3 ] && redis_mode=old || redis_mode=new
    [ ${#MASTER_NODE_LIST[@]} == 1 ] && values_replicas=1 || values_replicas=2

## 生成密码
    es_pass=$(</dev/urandom tr -dc A-Z-a-z | head -c10 | sed 's/[^a-zA-Z]//g')
    es_pass_64=$(echo ${es_pass} | base64)
    es_user_64=$(echo alaudaes | base64)
    redis_pass=$(</dev/urandom tr -dc A-Z-a-z | head -c10 | sed 's/[^a-zA-Z]//g')
    redis_pass_64=$(echo ${redis_pass} | base64)
    DB_USER_64=$(echo ${DB_USER} | base64)
    DB_PASSWORD_64=$(echo ${DB_PASSWORD} | base64)

## 生成redis.toml 文件
    cat <<EOF >/tmp/redis.toml
# db 密码
DB_PASSWORD="${DB_PASSWORD}"

# jakrio 密码
JAKIRO_PASSWORD='TVFcc]J%p35x/Z>'

# normal为单例，cluster为集群
REDIS_TYPE_READER="cluster"

# redis的DB name，只有在单例模式下才管用，集群模式会被忽略
REDIS_DB_NAME_READER=0

# 密码,如果没有密码则传空
REDIS_DB_PASSWORD_READER="${redis_pass}"
REDIS_DB_PASSWORD_WRITER="${redis_pass}"

# 最大连接数, 默认为32
REDIS_MAX_CONNECTIONS_READER=32

# redis key的前缀
REDIS_KEY_PREFIX_READER="alauda"

# 当用户屏蔽了CONFIG命令时,需要把这个值改为true，只有集群模式有这个变量
REDIS_SKIP_FULL_COVER_CHECK_READER=false

# redis连接和操作的超时时间为5秒
REDIS_TIMEOUT_READER=5
EOF
    if [ "${redis_mode}z" == "oldz" ]
    then
        echo "# ip，集群模式下IP会有多个，以逗号分割，IP数量和端口数量一致" >> /tmp/redis.toml
        echo 'REDIS_HOST_READER=["'${REDIS_NODE_LIST[0]}'","'${REDIS_NODE_LIST[1]}'","'${REDIS_NODE_LIST[2]}'","'${REDIS_NODE_LIST[0]}'","'${REDIS_NODE_LIST[1]}'","'${REDIS_NODE_LIST[2]}'"]' >> /tmp/redis.toml
        echo '# 端口，集群模式下端口会有多个，以逗号分割，IP数量和端口数量一致' >> /tmp/redis.toml
        echo 'REDIS_PORT_READER=["26379","26379","26379","26380","26380","26380"]' >> /tmp/redis.toml
    else
        echo "# ip，集群模式下IP会有多个，以逗号分割，IP数量和端口数量一致" >> /tmp/redis.toml
        echo 'REDIS_HOST_READER=["'${REDIS_NODE_LIST[0]}'","'${REDIS_NODE_LIST[1]}'","'${REDIS_NODE_LIST[2]}'","'${REDIS_NODE_LIST[3]}'","'${REDIS_NODE_LIST[4]}'","'${REDIS_NODE_LIST[5]}'"]' >> /tmp/redis.toml
        echo '# 端口，集群模式下端口会有多个，以逗号分割，IP数量和端口数量一致' >> /tmp/redis.toml
        echo 'REDIS_PORT_READER=["26379","26379","26379","26379","26379","26379"]' >> /tmp/redis.toml
    fi

    echo '$(whereis -b helm | awk '"'"{print \$2}"'"') install --set global.variable.redis_toml="'$(cat /tmp/redis.toml | base64 -w 0)'" --set global.db_user_64="'${DB_USER_64}'" --set global.db_passwd_64="'${DB_PASSWORD_64}'" --set global.es_user_64="'${es_user_64}'" --set global.es_passwd_64="'${es_pass_64}'" --set global.es_passwd="'${es_pass}'" --set global.variable.redis_passwd="'${redis_pass}'" --set global.variable.redis_passwd_base64="'${redis_pass_64}'" --set global.variable.vip='${DOMAIN_NAME}' --set global.variable.db_engine='${DB_ENGINE}' --set global.variable.db_host='${DB_HOST}' --set global.variable.db_port='${DB_PORT}' --set global.variable.db_user='${DB_USER}' --set global.variable.db_password='${DB_PASSWORD}' --set global.variable.es_node1='${LOG_NODE_LIST[0]}' --set global.variable.es_node2='${LOG_NODE_LIST[1]}' --set global.variable.es_node3='${LOG_NODE_LIST[2]}' --set global.variable.redis_node1='${REDIS_NODE_LIST[0]}' --set global.variable.redis_node2='${REDIS_NODE_LIST[1]}' --set global.variable.redis_node3='${REDIS_NODE_LIST[2]}' --set global.variable.redis_node4='${REDIS_NODE_LIST[3]}' --set global.variable.redis_node5='${REDIS_NODE_LIST[4]}' --set global.variable.redis_node6='${REDIS_NODE_LIST[5]}' --set global.variable.redis_node_num="'${#REDIS_NODE_LIST[@]}'" --set global.variable.redis_mode='${redis_mode} ' --set global.variable.chart_repo_url='${CHART_ENDPOINT}' --set global.variable.regrstry_ep='${REGISTRY_ENDPOINT}' --set global.registry='${REGISTRY_ENDPOINT}' --set replicas="'${values_replicas}'" --namespace '$(grep 'namespace:' /tmp/alaudaee_chart/ACE/values.yaml | awk '{print $2}') ' --name alauda-ace stable/ACE' >/tmp/helm_install.sh
    ${MASTER_SCP_COMMAND[0]} /tmp/helm_install.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
    if ${MASTER_SSH_COMMAND} '  bash /tmp/helm_install.sh  >/dev/null 2>&1'
    then
        log_print up_alauda_ee info 'Helm executed successfully, waiting for ACE to start successfully'
        log_print up_alauda_ee info 'Check every minutes, check 10 times in total, if ACE still does not start successfully, please check'
	sleep 10
        log_print up_alauda_ee info 'rebuild all pod'
	${MASTER_SSH_COMMAND} 'kubectl delete po -n alauda-system --all' >/dev/null 2>&1
        sleep 20
        for i in {1..11}
        do
            sleep 60
            if [ $i == 11 ] ; then log_print up_alauda_ee error 'ACE did not start successfully' ; exit 1 ; fi
            ${MASTER_SSH_COMMAND} '  $(whereis -b kubectl | awk "{print \$2}") get pod -n alauda-system -o wide |grep -E "architect|chen2|darchrow|davion|enigma|heimdall|jakiro|jakiro2|kaldr|krobelus|lanaya|lightkeeper2|lucifer|morgans|razzil|rubick|tiny|tiny-proxy|wisp|kunkka|furion|hydra|hedwig|redis|elasticsearch"' >/tmp/alaudaee_pod_list 2>/dev/null
            cat /tmp/alaudaee_pod_list | awk -F '[ /]+' '(NR>1 && $2!=$3){print}' >/tmp/alauda_pod_status
            if [ -s /tmp/alauda_pod_status ]
            then
                log_print up_alauda_ee warning 'The following pods did not start successfully'
                cat /tmp/alauda_pod_status
            else
                log_print up_alauda_ee success 'All pods started successfully'
                break
            fi
        done
    else
        log_print up_alauda_ee error "Helm executed failed, The commands executed are as follows: $(cat /tmp/helm_install.sh) "
        exit 1
    fi
## 检查 pod 是否全部启动，利用以前的脚本，所有的 pod 都检查，同时检查 node
    ${MASTER_SCP_COMMAND[0]} /tmp/check_k8s_cluster.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
    for i in {1..12}
    do
        log_print check_alauda info 'info Wait 60 seconds' ; sleep 60
        if ${MASTER_SSH_COMMAND} '  bash /tmp/check_k8s_cluster.sh 2>/dev/null'
        then
            log_print check_k8s success "all pod and node states are ready."
            break
        else
            log_print check_k8s error "status is abnormal"
            if [ $i -gt 10 ]
            then
                log_print check_k8s error "After ten checks, the pod status of the ACE is still not all normal. Please check" 
            fi
        fi
    done


## migrate
    log_print up_alauda_ee info "Execute the /tmp/migrate.sh script, Command is as follows: ${MASTER_SSH_COMMAND} '  bash /tmp/migrate.sh'"
    ${MASTER_SCP_COMMAND[0]} ${PWD_LOCAL}/migrate.sh ${MASTER_SCP_COMMAND[1]}:/tmp/ >/dev/null 2>&1
    if ${MASTER_SSH_COMMAND} '  bash /tmp/migrate.sh'
    then
        log_print up_alauda_ee info 'migrate success'
    else
        log_print up_alauda_ee error 'migrate error, deployment failed'
        exit 1
    fi
    log_print up_alauda_ee warning 'After the script is executed, it is not sure that all components migrate are all successful'
}

check_alauda_ee()
{
## 检查 ACE 是否正常
## server_check.sh    diagnose
:
}

clear_all()
{
    for i in 1 2 3 ; do log_print clear_all warning 'Clear all servers' ; sleep 2 ; done
    for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
    do
        set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
        IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')
        if [ "${SSH_PASSWD}z" != "z" ]
        then
            timeout --signal=9 5 sshpass -p "${SSH_PASSWD}" scp -P ${SSH_PORT} ${PWD_LOCAL}/clea*.sh ${SSH_USER}@${IP_ADDR}:/tmp/ && sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} '  bash /tmp/clear-all.sh'
        else
            timeout --signal=9 5 scp -P ${SSH_PORT} -i ${SSH_KEY_FILE} ${PWD_LOCAL}/clea*.sh ${SSH_USER}@${IP_ADDR}:/tmp/ && ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR} '  bash /tmp/clear-all.sh'
        fi
    done
}

add_something()
{
    log_print delpoy_alauda info 'sleep 60s, Waiting for redis cluster ready'
    sleep 60
    superuser_pass=$(</dev/urandom tr -dc A-Z-a-z | head -c10 | sed 's/[^a-zA-Z]//g')
    superuser_name=${ROOT_USERNAME}
    
## 创建根用户，添加 global 集群之类的操作
    log_print delpoy_alauda info 'create admin user'
    if curl -X POST \
         -H 'Content-Type:application/json' \
         -d '{
    "username":"'${superuser_name}'",
    "password":"'${superuser_pass}'",
    "realname":"alauda",
    "email":"alauda@alauda.io",
    "reference_code_id":"cn",
    "city":"beijing",
    "company":"alauda",
    "is_active": true,
    "informed_way":"others",
    "industry":"others",
    "mobile":"123123123123",
    "position":"beijing",
    "currency":"CNY"
}' http://${DOMAIN_NAME}:$(cat /tmp/alaudaee_chart/ACE/values.yaml | grep -A1 '^  jakiro:' | awk '/node_port_80/ {print $2}')/v1/auth/register/ >/dev/null 2>&1
    then
        log_print delpoy_alauda success "create admin user success. username:${superuser_name}   password:${superuser_pass}"
    else
        log_print delpoy_alauda error 'create admin user failure'
        exit 1
    fi
    sleep 3
## 获取k8s 集群的 token ，接入 global 集群
    global_token=''
    kubectl_path=$(${MASTER_SSH_COMMAND} 'echo $(whereis -b kubectl | awk "{print \$2}")')
    cluster_secret_name=$(${MASTER_SSH_COMMAND} "${kubectl_path} get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print \$1}'")
    log_print delpoy_alauda info 'Get the token of the global cluster'
    if global_token=$(${MASTER_SSH_COMMAND} "${kubectl_path} get secret -n kube-system ${cluster_secret_name} -o jsonpath='{.data.token}' | base64 -d")
    then
        log_print delpoy_alauda success 'Get the token of the global cluster success'
    else
        log_print delpoy_alauda error 'Get the token of the global cluster failure'
        exit 1
    fi
## 获取根用户的 token
    ace_user_token=''
    log_print delpoy_alauda info 'get alauda token'
    if ace_user_token=$(curl -fsSL -X POST \
      http://${DOMAIN_NAME}:$(cat /tmp/alaudaee_chart/ACE/values.yaml | grep -A1 '^  jakiro:' | awk '/node_port_80/ {print $2}')/v1/generate-api-token \
      -H 'content-type: application/json' \
      -d '{
        "email": "alauda@alauda.io",
        "username": "'${superuser_name}'",
        "password": "'${superuser_pass}'"
    }' | awk '{print $4}' | sed -e 's/"//g' -e 's/,//g')
    then
        log_print delpoy_alauda success "get ${superuser_name} token success"
    else
        log_print delpoy_alauda error "get ${superuser_name} token failure"
        exit 1
    fi
## 接入 global集群
    region_docker_version=$(${MASTER_SSH_COMMAND} docker version --format '{{.Server.Version}}')
    region_k8s_version=$(${MASTER_SSH_COMMAND} kubectl version --output=json | jq .serverVersion.gitVersion | sed 's/[^0-9.]//g')
    log_print delpoy_alauda info "add global region, docker=${region_docker_version} k8s=${region_k8s_version}"
    cat <<EOF >/tmp/add_region.json
{
  "display_name": "ACE",
  "name": "ace",
  "namespace": "${superuser_name}",
  "attr": {
    "cluster": {
      "nic": "${NET_DEV}"
    },
    "docker": {
      "path": "/var/lib/docker",
      "version": "${region_docker_version}"
    },
    "cloud": {
      "name": "PRIVATE"
    },
    "kubernetes": {
      "type": "original",
      "endpoint": "https://${K8S_CONTROL_LB}:6443",
      "token": "${global_token}",
      "version": "${region_k8s_version}",
      "cni": {
        "type": "flannel",
        "cidr": "10.199.0.0/16",
        "backend": "vxlan"
      }
    }
  },
  "features": {
  }
}
EOF
    if curl -X POST \
         -H "Authorization: Token ${ace_user_token}" \
         -H 'Content-Type:application/json' \
         --data @/tmp/add_region.json \
         http://${DOMAIN_NAME}:$(cat /tmp/alaudaee_chart/ACE/values.yaml | grep -A1 '^  jakiro:' | awk '/node_port_80/ {print $2}')/v2/regions/${superuser_name} >/dev/null 2>&1
    then
        log_print delpoy_alauda success 'Add global cluster success'
    else
        log_print delpoy_alauda error 'Add global cluster failure'
        exit 1
    fi

}

## update ats.cfg by actual env
set_ats_cfg()
{
    ATS_DIR=${PWD_LOCAL}/ats
    ATS_CFG_PATH=${ATS_DIR}/ats.cfg
    MASTER_NODE_LIST=''; SLAVE_NODE_LIST='';
    COMMON_PASS=''; COMMON_USER=''; COMMON_SSH_KEY=''; COMMON_PORT='';
    SPEC_LOGIN_NODES='';
    for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
    do
        SEPERATE_SIGNAL_MASTER=','; SEPERATE_SIGNAL_SLAVES=','; SEPERATE_SIGNAL_SPEC=',';
        ## 遍历 server_list.json 文件中配置的服务器信息
        set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file " ${SERVER_LIST_FILE} | sed 's/"//g')
        IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$(jq .[${i}].ssh_passwd ${SERVER_LIST_FILE} | sed 's/"//g')

        if [[ ${i} == 0 ]];then
        {
            COMMON_PASS=${SSH_PASSWD}
            COMMON_USER=${SSH_USER}
            COMMON_SSH_KEY=${SSH_KEY_FILE}
            COMMON_PORT=${SSH_PORT}
        }
        fi
        [[ "${MASTER_NODE_LIST}" == "" ]] && SEPERATE_SIGNAL_MASTER=''
        [[ "${SLAVE_NODE_LIST}" == "" ]] && SEPERATE_SIGNAL_SLAVES=''
        if [[ "${SSH_PASSWD}" != "${COMMON_PASS}" ]] || [[ "${SSH_USER}" != "${COMMON_USER}" ]]  || [[ "${SSH_KEY_FILE}" != "${COMMON_SSH_KEY}" ]];then
        {
            #ip=192.168.0.2 ansible_ssh_pass=12345,ip=192.168.8.3 ansible_ssh_private_key_file=/home/user1/.ssh/id_rsa
            [ "${SPEC_LOGIN_NODES}" == "" ] && SEPERATE_SIGNAL_SPEC=''
            [ "${SSH_PASSWD}" == "" ] && [ "${SSH_KEY_FILE}" != "" ] && SPEC_LOGIN_NODES="${SPEC_LOGIN_NODES}${SEPERATE_SIGNAL_SPEC}ip=${IP_ADDR} ansible_ssh_private_key_file=${SSH_KEY_FILE}"
            [ "${SSH_PASSWD}" != "" ] && [ "${SSH_KEY_FILE}" == "" ] && SPEC_LOGIN_NODES="${SPEC_LOGIN_NODES}${SEPERATE_SIGNAL_SPEC}ip=${IP_ADDR} ansible_ssh_pass=${SSH_PASSWD}"
        }
        fi
        [ $(jq ".[${i}].server_role.master" ${SERVER_LIST_FILE}) == true ] && MASTER_NODE_LIST="${MASTER_NODE_LIST}${SEPERATE_SIGNAL_MASTER}${IP_ADDR}"
        [ $(jq ".[${i}].server_role.master" ${SERVER_LIST_FILE}) == null ] && SLAVE_NODE_LIST="${SLAVE_NODE_LIST}${SEPERATE_SIGNAL_SLAVES}${IP_ADDR}"
    done
    ## 替换ats.cfg.tpl模版
    sed -i "s/{user}/${COMMON_USER}/g" ${ATS_CFG_PATH}
    sed -i "s/{password}/${COMMON_PASS}/g" ${ATS_CFG_PATH}
    sed -i "s/{port}/${COMMON_PORT}/g" ${ATS_CFG_PATH}
    sed -i "s#{private_key_file}#${COMMON_SSH_KEY}#g" ${ATS_CFG_PATH}
    sed -i "s/{masters}/${MASTER_NODE_LIST}/g" ${ATS_CFG_PATH}
    sed -i "s/{slaves}/${SLAVE_NODE_LIST}/g" ${ATS_CFG_PATH}
    sed -i "s/{spec_login_nodes}/${SPEC_LOGIN_NODES}/g" ${ATS_CFG_PATH}
    sed -i "s/{etcd_ips}/${MASTER_NODE_LIST}/g" ${ATS_CFG_PATH}
}

## check command
for i in sshpass jq
do
    if ! command_exists $i
    then
        log_print check_command error "$i command does not exist"
        exit 1
    fi
done

cp ${PWD_LOCAL}/checkdb /usr/bin
chmod a+x /usr/bin/checkdb
file_check
[ "${CLEAR_ALL}z" == "truez" ] && clear_all
server_check
[ "${OLNY_CHECK}z" == "truez" ] && exit 0
get_local_ip
## 判断必须的参数是否加上
if [ ! -d ${ACP_PWD_LOCAL} ] ; then log_print init error "Acp directory not found" ; exit 1 ; fi
if [ "${DOMAIN_NAME}z" == "nonez" ] && [ "${MAKE_LB}z" != "truez" ] ; then log_print init error "--alaudaee-domain-name parameter must not be empty" ; exit 1 ; fi
if [ "${MAKE_DB}z" != "truez" ]
then
    if [ "${DB_INFO}z" == "nonez" ]
    then
        log_print init error "--db-infoparameter must not be empty"
        exit 1
    else
        DB_PORT='' ; DB_PASSWORD='' ; DB_USER='' ; DB_HOST='' ; DB_ENGINE=''
        eval ${DB_INFO}
        if [ "${DB_PORT}z" == "z" ] || [ "${DB_PASSWORD}z" == "z" ] || [ "${DB_USER}z" == "z" ] || [ "${DB_HOST}z" == "z" ] || [ "${DB_ENGINE}z" == "z" ]
        then
            log_print init error "Parameter dd error, please refer to the example, the correct input paramete value"
            log_print init error "log_print init error: --db-info='DB_HOST=1.1.1.1;DB_PORT=222;DB_USER=root;DB_PASSWORD=asdfqwerhi29i38u;DB_ENGINE=postgresql "
            exit 1
        fi
    fi
fi
do_it init_base
## 是否需要部署 global 的 k8s 集群
if [ "${NOT_DEPLOY_K8S}z" == "truez" ]
then
## 如果不需要部署，检查提供的 k8s 集群是否安装好 helm，如果没部署 helm，部署
    log_print deploy_k8s info 'Do not deploy kubernets, use a successfully deployed k8s cluster'
    sleep 3
    log_print deploy_k8s warning 'Please confirm that the kubernets cluster has been deployed successfully and that the roles of all nodes in the cluster are consistent with those defined in the server_list.json file.'
    sleep 3
    log_print deploy_k8s warning 'press Enter key to continue'
    read
    log_print deploy_k8s info 'check helm '
    if ${MASTER_SSH_COMMAND} 'command -v helm >/dev/null 2>&1'
    then
        log_print deploy_k8s info 'helm command has been installed, check tiller server'
    else
        log_print deploy_k8s warning 'helm command is not installed, cp to master'
        ${MASTER_SCP_COMMAND[0]} ${ACP_PWD_LOCAL}/helm ${MASTER_SCP_COMMAND[1]}:/usr/local/sbin >/dev/null 2>&1
    fi
    if ${MASTER_SSH_COMMAND} '  $(command -v kubectl) get pod -n kube-system | grep -q tiller-deploy'
    then
        log_print deploy_k8s info 'Tiller server is running normally, add private chart repo'
    else
        log_print deploy_k8s warning 'Tiller server is not running, perform hellm initialization'
        if ${MASTER_SSH_COMMAND} "  \$(command -v helm) init --stable-repo-url ${CHART_ENDPOINT} -i ${REGISTRY_ENDPOINT}/claas/tiller:v2.10.0-rc.2 >/dev/null 2>&1"
        then
            log_print deploy_k8s success 'init helm success'
        else
            log_print deploy_k8s error 'init helm error'
            exit 1
        fi
    fi
    log_print deploy_k8s info 'add private chart repo'
    if ${MASTER_SSH_COMMAND} "  \$(command -v helm) repo add stable ${CHART_ENDPOINT} >/dev/null 2>&1"
    then
        log_print deploy_k8s info "add ${CHART_ENDPOINT} repo success"
    else
        log_print deploy_k8s error "add ${CHART_ENDPOINT} repo error"
        exit 1
    fi
else
    do_it install_k8s
    do_it set_ats_cfg
fi
do_it check_k8s
do_it config_k8s
do_it up_alauda_ee
do_it check_alauda_ee
if [ "${ROOT_USERNAME}z" == 'nonez' ]
then
    log_print delpoy_alauda info 'The root user is not created because the "--root-username" parameter is not used'
else
    do_it add_something
fi

