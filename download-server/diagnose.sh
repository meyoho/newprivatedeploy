#!/bin/bash
#  alauda-ee paas components health
#  use diagnose and ping api
#  version: v0.1
#  update:2018-10-15
#
########################################################
log_print()
{
    case "$2" in
        error)
            ## 红色
            log_color='\033[1;31m'
            log_level='ERROR   '
        ;;
        critical)
            ## 红色
            log_color='\033[1;31m'
            log_level='CRITICAL'
        ;;
        timeout)
            ## 红色
            log_color='\033[1;31m'
            log_level='TIMEOUT '
        ;;
        success)
            ## 绿色
            log_color='\033[1;32m'
            log_level='SUCCESS '
        ;;
        unknown)
            ## 紫色
            log_color='\033[0;35m'
            log_level='UNKNOWN '
        ;;
        warning)
            ## 紫色
            log_color='\033[0;35m'
            log_level='WARNING '
        ;;
        info)
            ## 绿色
            log_color='\033[0;32m'
            log_level='INFO    '
        ;;
        *)
            ## 红色
            log_color='\033[1;31m'
            log_level='ERROR   '
            log_message='The parameter of the log function is incorrect. Usage: log_print [type] [level] [message] '
    esac
    log_type=$1
    log_message=$3
    ## 黑色
    nc_color='\033[0m'
    ## 蓝色
    begin_color='\033[1;34m'
    for ((type_length = ${#log_type}; type_length < 15; type_length++))
    do
      log_type="${log_type} "
    done
    echo -e "${begin_color}[${log_type}]${log_color}[${log_level}]${nc_color}[$(date '+%Y%m%d-%T')][$(hostname -i | head -n 1)]\t${log_color}[${log_message}]${nc_color}" 
}


CHART_ENDPOINT=$(echo ${CHART_ENDPOINT} | sed 's#/$##')
if curl ${CHART_ENDPOINT}/health >/dev/null 2>&1
then
    if curl ${CHART_ENDPOINT}/health 2>/dev/null | jq >/dev/null 2>&1
    then
        log_print check success "chart repo ${CHART_ENDPOINT} healthy true"
    else
        log_print check error "chart repo ${CHART_ENDPOINT} healthy false"
        exit 1
    fi
else
    log_print check error "chart repo ${CHART_ENDPOINT} healthy false"
    exit 1
fi

CHART_ENDPOINT=$(echo ${CHART_ENDPOINT} | sed 's#/$##')
tgz_file=$(curl ${CHART_ENDPOINT}/index.yaml 2>/dev/null | grep -A1 '    urls:' | awk '/ACE/{print $2}' | head -n 1)
log_print check info 'download chart'
curl ${CHART_ENDPOINT}/${tgz_file} -O 2>/dev/null
tar zxf ${tgz_file##*/} -C /tmp
rm -rf /download/service_list.txt
log_print check info 'create service_list.txt'
for i in /tmp/alauda-ee/templates/*_service.yaml
do
    service_name=$(cat $i | awk '/^    service_name:/ {print $2}' | head -n 1)
    service_port=($(cat $i | awk '/^      port:/ {print $2}'))
    if [ ${#service_port[*]} -gt 1 ]
    then
        for j in ${service_port[@]}
        do
            if curl ${service_name}:$j/_diagnose 2>/dev/null | jq >/dev/null 2>&1
            then
                service_port=($j)
                break
            fi
        done
        [ ${#service_port[*]} -gt 1 ] && log_print check error "The following port of service ${service_name},failed to call diagnose api: ${service_port[*]}"
    fi 
    echo ${service_name}:${service_port[0]} >>/download/service_list.txt 
done
sed -i '/download-server/d' /download/service_list.txt
sed -i '/kaldr/d' /download/service_list.txt
#cat /download/service_list.txt

for i in $(cat /download/service_list.txt )
do
    curl $i/_ping >/dev/null 2>&1 && log_print check success "curl $i _ping api success" || log_print check error "curl $i _ping api fail"
done
sleep 3
log_print check info 'curl diagnose api'

for i in $(cat /download/service_list.txt )
do
    timeout -t 10 curl $i/_diagnose 2>/dev/null 1>/tmp/diagnose.json ; CURL_STATUS=$?
    RETURN_MESSAGE="$i"
    case "${CURL_STATUS}" in
        0)
            if jq . /tmp/diagnose.json >/dev/null 2>&1 
            then
                for j in $(jq '.details | keys' /tmp/diagnose.json 2>/dev/null | sed -e '1d' -e '$d' -e 's/,//g')
                do
                    RETURN_MESSAGE="${RETURN_MESSAGE} '$(jq ".details[$j].name" /tmp/diagnose.json 2>/dev/null | sed s/\"//g)--$(jq ".details[$j].status" /tmp/diagnose.json 2>/dev/null | sed s/\"//g)'"
                done
                if jq .status /tmp/diagnose.json 2>/dev/null | grep -q -i OK
                then
                    RETURN_LEVEL=success
                else
                    RETURN_LEVEL=warning
                fi

            else
                if cat /tmp/diagnose.json | grep -q '< HTTP/1.1 200 OK'
                then
                    RETURN_LEVEL=success
                else
                    RETURN_LEVEL=error
                fi
                RETURN_MESSAGE="${RETURN_MESSAGE} curl diagnose api ${RETURN_LEVEL}"
            fi

        ;;

        124)
            RETURN_LEVEL='timeout'
            RETURN_MESSAGE="${RETURN_MESSAGE} curl api timeout 10s  ${RETURN_LEVEL}"
        ;;

        *)
            RETURN_LEVEL=error
            RETURN_MESSAGE="${RETURN_MESSAGE} curl api failure. (api addr $i/_diagnose)"
        ;;

    esac
    log_print check ${RETURN_LEVEL} "${RETURN_MESSAGE}"
done

