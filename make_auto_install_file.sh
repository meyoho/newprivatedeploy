#!/bin/bash

for i in $@
do
    eval $i
done

pwd_local=$(dirname $0)
if [ "${node4}z" == "z" ]
then
    cp ${pwd_local}/server_list_3.json ${pwd_local}/server_list.json
else
    cp ${pwd_local}/server_list_6.json ${pwd_local}/server_list.json
fi
sed -i -e "s/1.1.1.1/$node1/" -e "s/2.2.2.2/$node2/" -e "s/3.3.3.3/$node3/" -e "s/4.4.4.4/$node4/" -e "s/5.5.5.5/$node5/" -e "s/6.6.6.6/$node6/" ${pwd_local}/server_list.json
if [ "${use_mysql}z" == "truez" ]
then
    echo "bash $(dirname $0)/up-alaudaee.sh --network-interface=eth0 --master-is-node --make-db --use-mysql --make-k8s-lb --root-username alauda"  >/root/auto_install_ace.sh
else
    echo "bash $(dirname $0)/up-alaudaee.sh --network-interface=eth0 --master-is-node --make-db --make-k8s-lb --root-username alauda" >/root/auto_install_ace.sh
fi
