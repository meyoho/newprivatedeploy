#!/bin/bash
test -d /alauda || mkdir /alauda
log_print()
{
    case "$2" in
        error)
            ## 红色
            log_color='\033[1;31m'
            log_level='ERROR   '
        ;;
        critical)
            ## 红色
            log_color='\033[1;31m'
            log_level='CRITICAL'
        ;;
        timeout)
            ## 红色
            log_color='\033[1;31m'
            log_level='TIMEOUT '
        ;;
        success)
            ## 绿色
            log_color='\033[1;32m'
            log_level='SUCCESS '
        ;;
        unknown)
            ## 紫色
            log_color='\033[0;35m'
            log_level='UNKNOWN '
        ;;
        warning)
            ## 紫色
            log_color='\033[0;35m'
            log_level='WARNING '
        ;;
        info)
            ## 绿色
            log_color='\033[0;32m'
            log_level='INFO    '
        ;;
        *)
            ## 红色
            log_color='\033[1;31m'
            log_level='ERROR   '
            log_message='The parameter of the log function is incorrect. Usage: log_print [type] [level] [message] '
    esac
    log_type=$1
    log_message=$3
    ## 黑色
    nc_color='\033[0m'
    ## 蓝色
    begin_color='\033[1;34m'
    for ((type_length = ${#log_type}; type_length < 15; type_length++))
    do
      log_type="${log_type} "
    done
    echo -e "${begin_color}[${log_type}]${log_color}[${log_level}]${nc_color}[$(date '+%Y%m%d-%T')][$(hostname)]\t${log_color}[${log_message}]${nc_color}" | tee -a /alauda/alauda.log
}
command_exists()
{
    command -v "$@" > /dev/null 2>&1
}

detection_os_version()
{
    lsb_dist=''
    dist_version=''
    if command_exists lsb_release
    then
        lsb_dist="$(lsb_release -si)"
    fi
    if [ -z "$lsb_dist" ] && [ -r /etc/lsb-release ]
    then
        lsb_dist="$(. /etc/lsb-release && echo "$DISTRIB_ID")"
    fi
    if [ -z "$lsb_dist" ] && [ -r /etc/SuSE-release ]; then
        lsb_dist='sles'
    fi
    if [ -z "$lsb_dist" ] && [ -r /etc/debian_version ]; then
        lsb_dist='debian'
    fi
    if [ -z "$lsb_dist" ] && [ -r /etc/fedora-release ]; then
        lsb_dist='fedora'
    fi
    if [ -z "$lsb_dist" ] && [ -r /etc/oracle-release ]; then
        lsb_dist='oracleserver'
    fi
    if [ -z "$lsb_dist" ]
    then
        if [ -r /etc/centos-release ] || [ -r /etc/redhat-release ]
        then
            lsb_dist='centos'
        fi
    fi
    if [ -z "$lsb_dist" ] && [ -r /etc/os-release ]
    then
        lsb_dist="$(. /etc/os-release && echo "$ID")"
    fi

    lsb_dist="$(echo "$lsb_dist" | tr '[:upper:]' '[:lower:]')"
    if [ "$lsb_dist" == "rhel" ]; then
        lsb_dist='centos'
    fi
    if [ "$lsb_dist" == "suse" ]; then
        lsb_dist='sles'
    fi
    case "$lsb_dist" in

        ubuntu)
            if command_exists lsb_release; then
                dist_version="$(lsb_release --codename | cut -f2)"
            fi
            if [ -z "$dist_version" ] && [ -r /etc/lsb-release ]; then
                dist_version="$(. /etc/lsb-release && echo "$DISTRIB_CODENAME")"
            fi
        ;;

        fedora|centos)
            dist_version="$(rpm -q --whatprovides redhat-release --queryformat "%{VERSION}\n" | sed 's/\/.*//' | sed 's/\..*//' | sed 's/Server*//' | sort -r | head -1)"
        ;;
        sles)
            dist_version="$(rpm -q --whatprovides sles-release --queryformat "%{VERSION}\n" | sed 's/\/.*//' | sed 's/\..*//' | sed 's/Server*//' | sort -r | head -1)"
        ;;

        *)
            log_print init error 'Does not support this operating system'
        ;;

    esac
}

install_docker()
{
    if command_exists docker
    then
        log_print init info "Docker already installed"
        if !docker ps >/dev/null 2>&1
        then
            log_print init error "Docker daemon is not running"
            log_print init info "Attempt to start the docker daemon"
            if !systemctl restart docker
            then
                log_print init error "Failed to start docker daemon"
                return 1
            fi
        fi
        if ! docker info | grep -q ${REGISTRY_ENDPOINT} 2>/dev/null
        then
            log_print init error "Configuration item insecure-registries in configuration file /etc/docker/daemon.json does not have ${REGISTRY_ENDPOINT}, please add"
            return 1
        fi
        return 0
    fi

    case "$lsb_dist" in

        centos)
            log_print init info "install docker for centos or redhat"
            cat <<EOF >/etc/yum.repos.d/alauda.repo
[alauda]
name=Alauda
baseurl=file://${ACP_PWD_LOCAL}/kaldr/yum
enabled=1
gpgcheck=0
EOF
            yum clean all && yum makecache
            if yum install -y --disablerepo=\* --enablerepo=alauda --setopt=obsoletes=0 docker-ce-cli-18.09.9-3.el7 docker-ce-18.09.9-3.el7
            then
                log_print init info "Successful installation docker"
                systemctl enable docker
            else
                log_print init error "Failed to install docker"
                return 1
            fi
#            if ! (lvdisplay | grep 'LV Name' | grep -q thinpool && lvdisplay | grep 'VG Name' | grep -q docker)
#            then
#                log_print init error "No found lvm volume,Please create"
#                return 1
#            fi

        ;;

        sles)
            log_print init info "install docker for SUSE Linux Enterprise Server 12"
            zypper addrepo -fc --no-gpgcheck ${ACP_PWD_LOCAL}/kaldr/zypper/ local
            zypper --non-interactive refresh
            if zypper --no-gpg-checks install --no-confirm --repo local docker
            then
                log_print init info "Successful installation docker"
                systemctl enable docker
            else
                log_print init error "Failed to install docker"
                return 1
            fi
            if [ $(df -Th /var/lib/docker| sed -n '2p'|awk '{print $2}') == "btrfs" ]
            then
                log_print init error "SuSE need file system btrfs, Partition /var/lib/docker deploy btrfs file system"
                return 1
            fi
        ;;

        *)
            log_print init error "hahahahah  bad bad, Does not support this operating system"
            return 1

        ;;

    esac
    test -d /etc/docker || mkdir /etc/docker
    cp ${DOCKER_CONFIG_FILE_TEMP} /etc/docker/daemon.json
    systemctl restart docker
}

get_local_ip()
{
## 获取本机 ip
    ip_addr_command=''
    log_message=''
    if [ "${NET_DEV}z" != "nonez" ]
    then
        ip_addr_command="ls ${NET_DEV}" ; log_message=", from the ${NET_DEV} NIC"
    fi
    LOCAL_IP=$(ip addr ${ip_addr_command} | grep -E 'inet [0-9]' | grep -v 127.0.0.1 | awk '{print $2}' | sed 's#/.*$##g' )
    if [ $(ip addr ${ip_addr_command} | grep -E 'inet [0-9]' | grep -v 127.0.0.1 | awk '{print $2}' | sed 's#/.*$##g' | wc -l) -gt 1 ]
    then
        log_print init error "Find multiple ip addresses, please specify network card ${NET_DEV}"
        return 1
    fi
    log_print init info "The local ip address is ${LOCAL_IP} ${log_message}"
}

create_daemon_json()
{
## 生成 docker 配置文件
    DOCKER_CONFIG_FILE_TEMP=${DOCKER_CONFIG_FILE_TEMP:-/tmp/daemon.json}
    if [ "${DOCKER_CONFIG_FILE}z" == "nonez" ]
    then
        case "$lsb_dist" in
    
            centos)
                cat <<EOF >${DOCKER_CONFIG_FILE_TEMP}
{
    "insecure-registries": [
        "${REGISTRY_ENDPOINT}"
    ],
    "storage-driver": "overlay2"
}
EOF
            ;;
    
            sles)
                cat <<EOF >${DOCKER_CONFIG_FILE_TEMP}
{
    "insecure-registries": [
        "${REGISTRY_ENDPOINT}"
    ],
    "storage-driver": "btrfs",
    "graph": "${DOCKER_PATH}"
}
EOF
            ;;
    
            *)
                log_print init error "hahahahah  bad bad, Does not support this operating system"
                return 1
    
            ;;
    
        esac
    else
        rm -rf ${DOCKER_CONFIG_FILE_TEMP}
        cp ${DOCKER_CONFIG_FILE} ${DOCKER_CONFIG_FILE_TEMP}
    fi

}

performance_optimization()
{
    test -d /alauda/old || mkdir -p /alauda/old
    case "$lsb_dist" in

        centos)
            chmod 777 /tmp
            log_print init info 'swap off'
            swapoff -a >/dev/null
            sed -i '/ swap /d' /etc/fstab
            cp /etc/security/limits.conf /alauda/old/limits.conf
            cat <<EOF >/etc/security/limits.conf
* soft nofile 819200
* hard nofile 819200
* soft noproc 819200
* hard noproc 819200
EOF
            ulimit -n 819200
            ulimit -u 819200
            cp /etc/sysctl.conf /alauda/old/sysctl.conf
            cat <<EOF >/etc/sysctl.conf
net.ipv4.tcp_max_orphans = 131072
net.ipv4.tcp_retries2 = 7
net.ipv4.ip_forward = 1
net.netfilter.nf_conntrack_max = 524288
net.bridge.bridge-nf-call-iptables = 1
net.bridge.bridge-nf-call-ip6tables = 1
kernel.sem = 250	32000	32	1024
fs.inotify.max_user_watches = 1048576
vm.dirty_background_ratio = 5
vm.dirty_ratio = 10
EOF
            log_print init info 'config sysctl'
            sysctl -p /etc/sysctl.conf >/dev/null
            systemctl enable systemd-sysctl.service

            if ! cat /lib/systemd/system/systemd-udevd.service | grep ^ExecStart | grep -q 'event-timeout=90'
            then
                sed -i 's#^ExecStart=\(.*$\)#ExecStart=\1 --event-timeout=90#' /lib/systemd/system/systemd-udevd.service || :
            fi
            log_print init info 'config and restart systemd-udevd'
            systemctl daemon-reload
            systemctl restart systemd-udevd.service >/dev/null 2>&1
    

            log_print init info "stop firewalld.service"
            systemctl disable firewalld.service || :
            systemctl stop firewalld.service >/dev/null 2>&1 || :
            log_print init info 'disabled selinux'
            setenforce 0 >/dev/null 2>&1 || :
            test -f /etc/selinux/config && sed -i 's/^SELINUX=.*$/SELINUX=disabled/' /etc/selinux/config

        ;;

        sles)
            :
        ;;

        *)
            log_print init error "hahahahah  bad bad, Does not support this operating system"
            return 1

        ;;

    esac
}

safety_optimization()
{
    test -d /alauda/old || mkdir -p /alauda/old
    case "$lsb_dist" in

        centos)
            :

        ;;

        sles)
            :
        ;;

        *)
            log_print init error "hahahahah  bad bad, Does not support this operating system"
            return 1

        ;;

    esac

}


do_it()
{
## 执行函数，并调用 log_print 打印出函数执行结果
    [ "${2}z" == "z" ] && log_message="start $1" || log_message="$2"
    check_type=$1
    log_print ${check_type} info "${log_message}"
    $1 && log_type=success || log_type=error ; log_print ${check_type} ${log_type} "${check_type} ${log_type}"
}


up_k8s()
{
    UP_K8S_COMMAN="--dockercfg=${DOCKER_CONFIG_FILE_TEMP} --network-opt='backend_type="${NETWORK_BACKEND}"' --network-opt='cidr="${NETWORK_CIDR}"'"
    [ "${USER_ALAUDA_KALDR}z" == "truez" ] || UP_K8S_COMMAN="${UP_K8S_COMMAN} --registry=${REGISTRY_ENDPOINT}"
    [ "${USER_ALAUDA_REGISTRY}z" == "truez" ] || UP_K8S_COMMAN="${UP_K8S_COMMAN} --pkg_repo=${KALDR_ENDPOINT}"
    if [ "${MASTER_LIST}z" == "nonez" ]
    then
        UP_K8S_COMMAN="$(command -v ake) up ${UP_K8S_COMMAN}"
    else
        UP_K8S_COMMAN="$(command -v ake) deploy ${UP_K8S_COMMAN} --masters='${MASTER_LIST}'"
        [ "${NODE_LIST}z" == "nonez" ] || UP_K8S_COMMAN="${UP_K8S_COMMAN} --nodes='${NODE_LIST}'"
    fi
    eval ${UP_K8S_COMMAN}

}

>/tmp/opt_error
eval set -- `getopt -o rkcmns --longoptions root-username:,use-mysql,debug,clear-all,acp-simple,make-db,make-k8s-lb,only-check,not-check-lb,user-alauda-registry,user-alauda-kaldr,user-alauda-chart,not-deploy-k8s,master-is-node,simple,dockercfg:,network-cidr:,network-backend:,network-interface:,acp-relative-path:,masters:,nodes:,kube_controlplane_endpoint:,alaudaee-domain-name:,db-info: -- "$@" 2>/tmp/opt_error`
if [ -s /tmp/opt_error ]
then
    log_print init error "Argument error! Use --help parameter for more information"
    return 1
fi
NET_DEV=none ; DB_INFO=none ; DOCKER_CONFIG_FILE=none ; MAKE_LB=none ; DOMAIN_NAME=none ; ROOT_USERNAME=none
SIMPLE_PLATFORM='true'
K8S_CONTROL_LB='none'
ACP_SIMPLE='none'
CLEAR_ALL='none'
NETWORK_BACKEND='vxlan'
NETWORK_CIDR='10.199.0.0/16'
while :
do
    case "$1" in
        -r|--user-alauda-registry) USER_ALAUDA_REGISTRY='true'; shift 1 ;; ## 不部署仓库，使用 index.alauda.cn
        -k|--user-alauda-kaldr) USER_ALAUDA_KALDR='true'; shift 1 ;;       ## 不部署源，使用 kaldr.alauda.cn
        -c|--user-alauda-chart) USER_ALAUDA_CHART='true'; shift 1 ;;       ## 不部署 chartmuseum ,使用ALAUDA_CHARTMUSEUM
        -m|--master-is-node) MASTER_IS_NODE='true'; shift 1 ;;             ## k8s 集群的 master ，同时作为计算节点使用
        -n|--not-deploy-k8s) NOT_DEPLOY_K8S='true'; shift 1 ;;             ## 不部署 k8s 集群，server_list.json 中记录的服务器，就是已经部署成功的 k8s 集群的所有服务器
        -s|--simple) SIMPLE_PLATFORM='true'; shift 1 ;;                    ## 不部署镜像仓库、chart repo 和软件源
        --clear-all) CLEAR_ALL='true'; shift 1 ;;                          ## 清除 server_list.json 文件中定义的所有非服务器
        --debug) DEBUG='true'; shift 1 ;;                                  ## 调用 ake 命令部署集群的时候，增加 debug 参数，输出更多信息
        --acp-simple) ACP_SIMPLE='true'; shift 1 ;;                        ## 不部署小平台的 devops、dashboard 之类的 chart
        --make-db) MAKE_DB='true'; shift 1 ;;                              ## docker run 的方式起 postgres 数据库，仅仅用于测试
        --make-k8s-lb) MAKE_LB='true'; shift 1 ;;                          ## 在本机运行 haproxy，作为 高可用k8s api 的 lb，（仅用于测试，不得用于生产）
        --only-check) OLNY_CHECK='true'; shift 1 ;;                        ## 只检查服务器
        --not-check-lb) NOT_CHECK_LB='true'; shift 1 ;;                    ## 不检查 lb
        --use-mysql) USE_MYSQL='true'; shift 1 ;;                          ## 如果添加--make-db，那么再指定这个参数后，就起 mysql 数据库
        --kube_controlplane_endpoint) K8S_CONTROL_LB=$2; shift 2 ;;        ## k8s api lb 的地址
        --root-username) ROOT_USERNAME=$2; shift 2 ;;                      ## 指定根用户名
        --acp-relative-path) ACP_PATH=$2; shift 2 ;;                       ## 小平台安装目录的相对路径
        --network-backend) NETWORK_BACKEND=$2; shift 2 ;;                  ## 网络模式，默认 vsxlan，详见 man ake
        --network-cidr) NETWORK_CIDR=$2; shift 2 ;;                        ## cidr ，默认使用10.199.0.0/16 详见 man ake
        --network-interface) NET_DEV=$2; shift 2 ;;                        ## 使用那块网卡
        --dockercfg) DOCKER_CONFIG_FILE=$2; shift 2 ;;                     ## 指定 docker 的配置文件
        --alaudaee-domain-name) DOMAIN_NAME=$2; shift 2 ;;                 ## ACE 的域名，客户、region 都通过这个域名来访问 global
        --db-info) DB_INFO=$2; shift 2 ;;                                  ## 数据库信息, 例子：--db-info='DB_HOST=1.1.1.1;DB_PORT=222;DB_USER=root;DB_PASSWORD=asdfqwerhi29i38u;DB_ENGINE=postgresql'
        --) shift; break ;;
    esac
done
