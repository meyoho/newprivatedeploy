#!/bin/bash
## alauda mini platform installation script
## htwang@alauda.io
## 2018-6-16
## v0.1

set -e

## 设置变量
ALAUDA_CHARTMUSEUM='http://chartmuseum.alaudak8s.haproxy-150-109-47-155-alaudak8s.myalauda.cn/'
## 判断是不是该调用function.sh
if ! command_exists do_it 2>/dev/null
then
    . function.sh "$@"
fi

get_local_ip
ACP_PWD_LOCAL_RUN=$(pwd)
[[ $0 =~ "/" ]] && cd ${0%/*}
ACP_PWD_LOCAL=${ACP_PWD_LOCAL:-$(pwd)}
cd $ACP_PWD_LOCAL_RUN
REGISTRY_DIRECTORY="${ACP_PWD_LOCAL}/registry"
REGISTRY_IMAGE="registry:latest"
REGISTRY_PORT=60080
REGISTRY_ENDPOINT="${LOCAL_IP}:${REGISTRY_PORT}"
KALDR_PORT="7000"
KALDR_IMAGE="${REGISTRY_ENDPOINT}/alaudaorg/kaldr:190525"
KALDR_ENDPOINT="http://${LOCAL_IP}:${KALDR_PORT}"
DOCKER_CONFIG_FILE_TEMP="/alauda/daemon.json"
CHART_PORT=8088
CHART_DIRECTORY="${ACP_PWD_LOCAL}/chartmuseum"
CHART_IMAGE="${REGISTRY_ENDPOINT}/chartmuseum:latest"
CHART_USER="chartmuseum"
CHART_PASSWD="chartmuseum"
CHART_ENDPOINT="http://${CHART_USER}:${CHART_PASSWD}@${LOCAL_IP}:${CHART_PORT}/"
test -d /alauda || mkdir /alauda

## 安装镜像仓库
install_registry()
{
    cat <<EOF >/alauda/.run_registry.sh
    docker load -i ${ACP_PWD_LOCAL}/registry.tar
    docker run -d \
        --restart=always \
        --name pkg-registry \
        -p ${REGISTRY_PORT}:5000 \
        -v ${REGISTRY_DIRECTORY}:/var/lib/registry \
        ${REGISTRY_IMAGE}
EOF
    if [ "${USER_ALAUDA_REGISTRY}z" == "truez" ]
    then
        log_print init info "Use a alauda registry, without deploying a local registry"
        REGISTRY_ENDPOINT=index.alauda.cn
        return 0
    else
        log_print init info "install alauda registry"
        if bash /alauda/.run_registry.sh >/dev/null 2>&1
        then
            log_print init success "install pkg-registry success, alauda registry endpoint ${REGISTRY_ENDPOINT}"
        else
            log_print init error 'install pkg-registry failure, Run /alauda/.run_registry.sh script to try to deploy test manually'
            exit 1
        fi
    fi
}


## 安装软件源
install_yum_repo()
{
    cat <<EOF >/alauda/.run_kaldr.sh
    docker run -d \
        --restart=always \
        --name=yum \
        -e KALDR_HOST="${LOCAL_IP}:${KALDR_PORT}" \
        -p 7000:7000 \
        ${KALDR_IMAGE}
EOF
    if [ "${USER_ALAUDA_KALDR}z" == "truez" ]
    then
        log_print init info "Use a alauda repo, without deploying a local repo"
        KALDR_ENDPOINT=http://kaldr.alauda.cn
    else
        log_print init info "install alauda repo"
        if bash /alauda/.run_kaldr.sh >/dev/null 2>&1
        then
            log_print init success "install alauda repo success, alauda repo endpoint ${KALDR_ENDPOINT}"
        else
            log_print init error 'install alauda repo failure, Run /alauda/.run_kaldr.sh script to try to deploy test manually'
            exit 1
        fi
    fi
}

## cp ake 二进制文件到/usr/bin
install_ake()
{
    command -v ake >/dev/null 2>&1 && rm -rf $(command -v ake)
    cp ${ACP_PWD_LOCAL}/ake /usr/bin 
    chmod +x /usr/bin/ake
    rm -rf /alauda/clea*
    cp ${ACP_PWD_LOCAL}/clea* /alauda
}


## 安装 chart 源
install_chart_repo()
{
    cat <<EOF >/alauda/.run_chart_repo.sh
        docker run -d \
          --restart=always \
          --name chart-repo \
          -e PORT=8080 \
          -e DEBUG=1 \
          -e STORAGE="local" \
          -e STORAGE_LOCAL_ROOTDIR="/data" \
          -e BASIC_AUTH_USER="${CHART_USER}" \
          -e BASIC_AUTH_PASS="${CHART_PASSWD}" \
          -p ${CHART_PORT}:8080 \
          -v ${CHART_DIRECTORY}:/data \
          ${CHART_IMAGE}
EOF
    if [ "${USER_ALAUDA_CHART}z" == "truez" ]
    then
        log_print init info "Use a alauda chart repo, without deploying a local repo"
        CHART_ENDPOINT=${ALAUDA_CHARTMUSEUM}
    else
        log_print init info "install aks chart repo"
        if bash /alauda/.run_chart_repo.sh >/dev/null 2>&1
        then
            log_print init success "install aks chart repo success, chart repo endpoint ${CHART_ENDPOINT}"
        else
            log_print init error 'install aks chart repo failure, Run /alauda/.run_chart_repo.sh script to try to deploy test manually'
            exit 1
        fi
    fi
}

test -d /alauda || mkdir /alauda
\cp -f -r -a ${ACP_PWD_LOCAL}/cleanup* /alauda
log_print init info "install the heml command"
#\cp -f -r -a ${ACP_PWD_LOCAL}/helm /usr/bin/


detection_os_version
create_daemon_json
install_docker
if [ $(docker ps -qa | wc -l) -gt 1 ]
then
    log_print init error 'Found that the machine has a container running, please delete all containers'
    exit 1
fi

install_registry
sleep 5
install_yum_repo
install_ake
install_chart_repo

[ "${SIMPLE_PLATFORM}z" == "truez" ] || up_k8s

echo -e "KALDR_ENDPOINT=${KALDR_ENDPOINT}\nREGISTRY_ENDPOINT=${REGISTRY_ENDPOINT}\nCHART_ENDPOINT=${CHART_ENDPOINT}" > /alauda/install_info
