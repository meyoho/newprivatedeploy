#!/bin/bash
# 1 4 * * * curl https://s3.cn-north-1.amazonaws.com.cn/alauda-ops/cluster/gc.sh -o /tmp/gc.sh ;  bash /tmp/gc.sh >>/var/log/mathilde/gc.log 2>&1

LOG_FILE="/var/log/mathilde/gc.log"
test -d /var/log/mathilde || mkdir -p /var/log/mathilde
find /var/log/mathilde  -type f -mtime +7  -exec rm {} \;

echo "Start executing gc script"

clear_dir()
{
    if [ ! -d $1 ]
    then
        echo "==== $1 not exists"
        return
    fi
    ls $1/*.gz
    rm -rf $1/*.1 $1/*.2 $1/*.gz
    case "$2" in

        list_files)

            shift 2
            for i in $@
            do
                echo "clear $1/${i}"
                test -f $1/$i && echo '' >$1/$i
            done

        ;;

        all_files)

            for i in $(find $1)
            do
                echo "clear $1/${i}"
                test -f $i && echo '' >$i
            done

        ;;

        *)
            echo 'bad'
       
        ;;

    esac
}

clear_docker_dir()
{
    for i in $(find $1/containers -name "*.log")
    do
        echo $i
        echo '' >$i
    done
    
}
## kill gc.sh
for i in $(ps -A -ostat,pid,command | grep gc.sh | grep -v grep | awk '{print $2}')
do
    for j in $(ps -A -o pid,ppid | awk -v p_pid="$i" '{if($2==p_pid){print $1}}')
    do
        [ "$$" == "$j" ] && continue
        kill -9 $j
    done
done

## kill zombie
for i in $(ps -A -ostat,pid | grep -i ^z | awk '{print $2}')
do
    kill -9 $i
done

## clear cni
for hash in $(tail -n +1 /var/lib/cni/networks/cbr0/* | grep '^[A-Za-z0-9]*$' | cut -c 1-8)
do
    if [ -z $(docker ps -a | grep $hash | awk '{print $1}') ]
    then
        grep -ilr $hash /var/lib/cni/networks/cbr0/*
    fi
done | xargs rm

## delete pod ( Terminating & Evicted )
kubectl get pod --all-namespaces | awk '{if ($6 ~/^[0-9]*d/) print }' | awk '{if ($4=="Terminating" || $4=="Evicted" || $4=="Completed") print $1" "$2}' | while read line
do
    timeout 60 kubectl delete pod -n ${line} --force --grace-period=0
done

## delete exited and dead containers
timeout 60 docker rm -f $(docker ps -a --filter="status=exited" | grep -v -E "second ago|seconds ago|minute ago|minutes ago" | awk '{ print $NF }')
timeout 60 docker rm -f $(docker ps -a --filter="status=dead" | awk '{ print $NF }')

## delete container that created 1000 seconds and is still in the "created" state
docker ps --filter status=created --format "table {{.ID}}\t{{.CreatedAt}}" | sed 1d | while read line
do
    container_id=$(echo ${line} | awk '{print $1}')
    if [ $(($(date "+%s")-$(date -d "$(echo ${line} | awk '{$1=""""; print}' | sed 's/CST$//g')" "+%s"))) -gt 1000 ]
    then
        timeout 60 docker rm -f ${container_id}
    fi
done

## clear images
FILTER_IMGS="clair|hours ago|kube-|minutes ago|seconds ago|kB|rattletrap|techies|kunkka|hour ago|minute ago|second ago"
timeout 60 docker rmi $(docker images | grep -v -E "${FILTER_IMGS}" | awk '{print $1":"$2}')
timeout 60 docker rmi $(docker images | grep -v -E "${FILTER_IMGS}" | awk '{print $3}')

## clear docker volume
#timeout 60 docker volume rm $(docker volume ls -qf dangling=true)

## Clear the pod directory where the container has been deleted
#docker ps -a > /tmp/containers_list
#for i in /var/lib/kubelet/pods/*
#do
#    if ! grep -q ${i##*/} /tmp/containers_list
#    then
#        for mnt in $(df | grep $i | awk '{print $1}')
#        do
#            umount $mnt
#        done
#        rm -rf $i
#    fi
#done

## clear nevermore log
#docker exec nevermore bash -c echo "">/var/log/td-agent/td-agent.log
#docker exec nevermore bash -c echo "">/var/log/mathilde/nevermore.log

## clear log
clear_dir /var/log list_files auth.log syslog cloud-init.log kern.log messages
clear_dir /var/log/apt list_files
clear_dir /var/log/chronos list_files output.log
clear_dir /var/log/upstart list_files docker.log
clear_dir /var/log/nginx list_files access.log
clear_dir /var/log/mesos all_files
clear_dir /var/log/spectre all_files
clear_dir /var/log/td-agent all_files
clear_dir /var/log/mathilde all_files
clear_dir /var/log/mathilde/ddserver all_files
clear_dir /var/log/uwsgi all_files
clear_docker_dir /var/lib/docker
clear_docker_dir /mnt/docker
clear_docker_dir /data/docker
clear_docker_dir /data2/docker

echo "Gc script execution completed"


