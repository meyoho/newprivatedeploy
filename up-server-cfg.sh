#!/bin/bash
mkdir /tmp/set-alauda
cp function.sh /tmp/set-alauda

cat <<EOF >/tmp/set-alauda/set.sh
#!/bin/bash

if [ ! -f /tmp/function.sh ]
then
    echo 'not find function.sh '
    exit 1
fi
. /tmp/function.sh

detection_os_version
performance_optimization
safety_optimization

EOF


file_check()
{
    ## 判断 server_list 文件是否存在及文件格式是否合法
    check_type='check_file'
    if [ -f ${SERVER_LIST_FILE} ]
    then
        if jq '.' ${SERVER_LIST_FILE} >/dev/null 2>&1
        then
            RETURN_LEVEL='info'
            RETURN_MESSAGE="${SERVER_LIST_FILE} file format is correct"
        else
            RETURN_LEVEL='error'
            RETURN_MESSAGE="${SERVER_LIST_FILE} file format is incorrect"
        fi
    else
        RETURN_LEVEL='error'
        RETURN_MESSAGE="${SERVER_LIST_FILE} file does not exist"
    fi
    log_print ${check_type} ${RETURN_LEVEL} "${RETURN_MESSAGE}"
    if [ ${RETURN_LEVEL} == error ] ; then exit 1 ; fi
}

set_server()
{

## 检查Server_list 文件中的服务器能否登录，如果用户名非 root ，检查是否具备免密 sudo 权限，检查操作系统配置是否符合要求
    check_node_env=0
    for i in $(jq 'keys | .[]' ${SERVER_LIST_FILE})
    do
        check_type='set_node_os'
## 遍历 server_list.json 文件中配置的服务器信息
        set -- $(jq ".[${i}].ip_addr , .[${i}].ssh_user , .[${i}].ssh_port , .[${i}].ssh_key_file , .[${i}].ssh_passwd" ${SERVER_LIST_FILE} | sed 's/"//g')
        IP_ADDR=$1 ; SSH_USER=$2 ; SSH_PORT=$3 ; SSH_KEY_FILE=$4 ; SSH_PASSWD=$5
## 生成 ake 参数
        if [ "${SSH_PASSWD}z" != "z" ]
        then
## 判断能否 ssh 登录，如果可以，检查这台服务器
            if timeout --signal=9 5 sshpass -p "${SSH_PASSWD}" scp -o StrictHostKeyChecking=no -P ${SSH_PORT} /tmp/set-alauda/* ${SSH_USER}@${IP_ADDR}:/tmp/ >/dev/null 2>&1
            then
                SSH_LOGIN_RETURN_LEVEL=success
                if [ ${SSH_USER} == root ]
                then
                    sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} bash /tmp/set.sh 2>/dev/null || check_node_env=1
                else
                    if timeout --signal=9 5 sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} sudo ls / >/dev/null 2>&1
                    then
                        sshpass -p "${SSH_PASSWD}" ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} sudo bash /tmp/set.sh 2>/dev/null || check_node_env=1
                    else
                        log_print ${check_type} error "User ${SSH_USER} does not have sudo permission on server ${IP_ADDR}"
                        check_node_env=1
                    fi
                fi
            else
                SSH_LOGIN_RETURN_LEVEL=error ; check_node_env=1
            fi
        else
            if timeout --signal=9 5 scp -o StrictHostKeyChecking=no -P ${SSH_PORT} -i ${SSH_KEY_FILE} /tmp/set-alauda/* ${SSH_USER}@${IP_ADDR}:/tmp/ >/dev/null 2>&1
            then
                SSH_LOGIN_RETURN_LEVEL=success
                if [ ${SSH_USER} == root ]
                then
                    ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR} bash /tmp/set.sh 2>/dev/null || check_node_env=1
                else
                    if timeout --signal=9 5 ssh -l ${SSH_USER} -p ${SSH_PORT} ${IP_ADDR} sudo ls / >/dev/null 2>&1
                    then
                        ssh -p ${SSH_PORT} -i ${SSH_KEY_FILE} -l ${SSH_USER} ${IP_ADDR} sudo bash /tmp/set.sh 2>/dev/null || check_node_env=1
                    else
                        log_print ${check_type} error "User ${SSH_USER} does not have sudo permission on server ${IP_ADDR}"
                        check_node_env=1
                    fi
                fi
            else
                SSH_LOGIN_RETURN_LEVEL=error ; check_node_env=1
            fi
        fi
        RETURN_MESSAGE="ssh login ${IP_ADDR} ${SSH_LOGIN_RETURN_LEVEL}"
        check_type='check_login'
        log_print ${check_type} ${SSH_LOGIN_RETURN_LEVEL} "${RETURN_MESSAGE}"
    done
## 检查完毕，检查结果
    if [ "${check_node_env}" == "1" ]
    then
        log_print ${check_type} error 'Some server set failed' ;  exit 1
    else
        log_print ${check_type} info 'All servers set through'
    fi
}

. $(pwd)/function.sh
SERVER_LIST_FILE=${SERVER_LIST_FILE:-$(pwd)/server_list.json}
file_check
set_server
