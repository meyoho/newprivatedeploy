# ACE Test Chart

## 目前安装包一般都在init的机器上，解压后所在路径在:/root/mnt/package/ace/ace_pack_20181230/chart/e2e-test;（安装包日期可以会有不同）

## 参数介绍

* REGISTRY=10.0.128.184:60080   global registry  `必填项`
* API_URL=http://10.0.129.106:32001     jakiro url  `必填项`
* ACCOUNT=alauda   根账号用户名 `必填项`
* PASSWORD=alauda  根账号密码   `必填项`
* REGION_NAME=access   集群名称 `必填项(如果集群不存在，私有发版环境和staging环境会新建机器部署集群,客户环境需要根据提供的VM信息部署集群)`
* REGISTRY_NAME=access  registry名称  `必填项`
* JENKINS_ENDPOINT=http://10.0.128.68:31252   jenkins地址(jenkins安装文档：http://confluence.alaudatech.com/pages/viewpage.action?pageId=27167746）
* JENKINS_USER=admin   jenkins用户名
* JENKINS_TOKEN=a71be439a7685a1cfd2f17d631a9cfea  jenkins用户名对应的token
* SONAR_ENDPOINT=http://192.144.148.212:10007     sonarqube地址
* SONAR_TOKEN=94df71bcca7d1e03cecee2222e4afa6047618533   sonarqube token
* SVN_REPO=http://192.144.148.212:10009/alauda_test/   svn代码仓库地址（需要有测试代码：http://confluence.alaudatech.com/pages/viewpage.action?pageId=6815766）
* SVN_USERNAME=User_Name-01         svn对应的用户名
* SVN_PASSWORD=alauda_Test-!@#      svn对应的密码
* GIT_REPO=http://192.144.148.212:10008/root/test123   gitlab代码仓库地址（需要有测试代码：http://confluence.alaudatech.com/pages/viewpage.action?pageId=6815766）
* GIT_USERNAME=root        gitlab用户名
* GIT_PASSWORD=07Apples    gialab密码
* CASE_TYPE=BAT            测试哪些用例 如果没有部署jenkins等第三方资源指定为BAT，如果jenkins,gitlab,svn,alb,监控,存储等第三方资源部署好指定为ace，会运行更多测试用例
* ENV=private              可指定为staging，private，customer。customer需要提供虚拟机信息，不会新建机器
* VM_IPS=""                虚拟机地址，格式为："1.1.1.1;2.2.2.2"  `客户环境是必填项`
* VM_USERNAME=root         虚拟机登录用户名  `客户环境是必填项`
* VM_PASSWORD=07Apples     虚拟机登录密码  `客户环境是必填项(与VM_PEM二选一)`
* VM_PEM=/root/key.pem     虚拟机登录的key，如果key不存在就是用密码登录测试  `客户环境是必填项(与VM_PASSWORD二选一)`
* REPORT_PATH=/tmp/e2e-test 测试报告挂载的本地路径 

* Alb 部署文档:http://confluence.alaudatech.com/pages/viewpage.action?pageId=29428188
* 存储部署文档:http://confluence.alaudatech.com/pages/viewpage.action?pageId=27174754
* 监控部署文档:http://confluence.alaudatech.com/pages/viewpage.action?pageId=31917836

## 首先根据参数介绍吧对应参数在Makefile里做相应的修改，然后执行 make install即可开始测试，通过查看alauda-system命名空间下的带e2e-test的pod的日志查看执行过程，完成后在pod所在机器上的指定路径下可以查看测试报告。如果想要再次执行首先需要执行make del吧之前的chart release删除，然后再次执行make install
## 如果当前集群没有安装helm 也可以使用docker run的方式运行测试，还是需要根据参数介绍吧对应参数在Makefile里作相应的修改，然后执行make docker-run即可
