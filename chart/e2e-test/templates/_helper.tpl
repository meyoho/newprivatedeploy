{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "e2e-test.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "e2e-test.image" -}}
{{- $registryAddress :=  .Values.global.registry -}}
{{- $repositoryName := .Values.images.e2etest.repository -}}
{{- $tag := .Values.images.e2etest.tag -}}
{{- printf "%s/%s:%s" $registryAddress $repositoryName $tag -}}
{{- end -}}
