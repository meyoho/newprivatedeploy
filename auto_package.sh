#!/bin/bash

node_user_name=root
branch=none
single=none
package_file=none
deploy=false ; node1=none ; node2=none ; node3=none ; node4=none ; node5=none ; node6=none
project=''
project_id=''
# 执行参数，让执行脚本时，带的参数生效
for i in $@
do
    eval $i
done
tell(){
curl -H "Content-Type: application/json" -d '{"msgtype": "text","text": {"content": "'"$1"'" }}' 'https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=c3f4ddf0-a6be-41d9-bba8-4c97fe532ea7'
}

if [ "${project}z" == "z" ]
then
    tell '必须定义项目，project 变量不得为空'
    exit 1
fi
if [ "${project_id}z" == "z" ]
then
    tell '必须定义成本，project_id 变量不得为空，研发=1142021   运维=1142020  QA=1126842'
    exit 1
fi
if echo ${project} | grep -q -E '^[A-Za-z][A-Za-z0-9!-]{1,18}[A-Za-z0-9]$'
then
    :
else
    tell 'project 变量 只能是字母、数字和短横线，且不能超过18个字符'
    exit 1
fi

install_ace()
{
# 自动部署 ace 的函数
    if ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') ls /root/mnt/package/ace/ace_pack_*/ | grep -q auto_package.sh
    then
        make_auto_install_file=$(ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') ls /root/mnt/package/ace/ace_pack_*/make_auto_install_file.sh)
        ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') mkdir /root/.ssh
        ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') chmod 0700 /root/.ssh
        scp -i ~/.ssh/tx ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}'):/root/.ssh/id_rsa
        ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') chmod 0600 /root/.ssh/id_rsa
        ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') bash ${make_auto_install_file} use_mysql=true node1=$(cat node_list | awk '/m-1/ {print $3}') node2=$(cat node_list | awk '/m-2/ {print $3}') node3=$(cat node_list | awk '/m-3/ {print $3}') node4=$(cat node_list | awk '/n-1/ {print $3}') node5=$(cat node_list | awk '/n-2/ {print $3}') node6=$(cat node_list | awk '/n-3/ {print $3}')
        json_list_file=$(ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') ls /root/mnt/package/ace/ace_pack_*/server_list.json)
        tell "server_list.json 文件内容 $(ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') cat ${json_list_file} )"
        if [ "${single}z" == "truez" ]
        then
            ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') bash ${make_auto_install_file%/*}/up-single.sh --network-interface=eth0
        else
            ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') bash /root/auto_install_ace.sh
        fi
    else
        tell '找不到安装目录，部署失败'
        rm_node
        exit 1
    fi
}

rm_node()
{

    tell "安装日志如下：$(ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') cat /alauda/alauda.log | tail)"
    for i in $(cat node_list | awk '{print $1}')
    do
        tccli cvm TerminateInstances --InstanceIds '["'$i'"]' --region ap-chongqing
    done
    rm -rf node_list
}

write_node_info()
{
    InstanceId=$1
    PrivateIp=null ; PublicIp=null
    echo "${InstanceId} init ${PrivateIp}  ${PublicIp}" >>node_list
    sleep 20
    tccli cvm DescribeInstances --InstanceIds '["'$InstanceId'"]' --region ap-chongqing >temp.json
    if jq . temp.json >/dev/null 2>&1
    then
        PrivateIp=$(jq '.InstanceSet[0].PrivateIpAddresses' temp.json | sed -e '1d' -e '$d' -e 's/^ *//' -e 's/"//g')
        PublicIp=$(jq '.InstanceSet[0].PublicIpAddresses' temp.json | sed -e '1d' -e '$d' -e 's/^ *//' -e 's/"//g')
    else
        tell '获取机器信息失败'
        rm_node
        exit 1
    fi
    if ! echo ${PrivateIp} | grep -q '[0-9]'
    then
        tell '获取机器信息失败'
        rm_node
        exit 1
    fi
    if ! echo ${PublicIp} | grep -q '[0-9]'
    then
        tell '获取机器信息失败'
        rm_node
        exit 1
    fi
    sed -i "/$InstanceId/d" node_list
    echo "${InstanceId} $2 ${PrivateIp}  ${PublicIp}" >>node_list
}


mk_node()
{
    rm -rf temp.json node_list ace-node-return
    for i in init m-1 m-2 m-3
#    for i in init m-1 m-2 m-3 n-1 n-2 n-3
    do
        [ "${project}z" == "z" ] || i=${project}-$i
## centos 7.4 img-8toqc6s3  centos 7.5 img-oikl1tzv SLES 12 img-d5304izr
	#--DataDisks '[{"DiskType":"CLOUD_PREMIUM","DiskSize":50}]' \
	qcloudcli cvm RunInstances  --Placement '{"Zone":"ap-chongqing-1","ProjectId":'${project_id}'}' --ImageId img-oikl1tzv --InstanceType SA1.2XLARGE16\
        --SystemDisk '{"DiskType":"CLOUD_PREMIUM","DiskSize":100}'  \
	--DataDisks '[{"DiskType":"CLOUD_PREMIUM","DiskSize":50}]' \
        --VirtualPrivateCloud '{"VpcId":"vpc-c9el9wfx","SubnetId":"subnet-6ohk5y8a"}' \
        --InternetAccessible '{"InternetChargeType":"TRAFFIC_POSTPAID_BY_HOUR","InternetMaxBandwidthOut":100,"PublicIpAssigned":"TRUE"}' \
        --InstanceCount 1  \
	--HostName "$i" \
	--InstanceName "$i-$(date +%Y%m%d)" \
        --UserData "IyEvYmluL2Jhc2gKL3Vzci9iaW4vd2dldCAtUCAvdG1wIGh0dHA6Ly8xMTguMjQuMjMzLjI4OjgwODAvemFiYml4LnNoICYmIC9iaW4vYmFzaCAvdG1wL3phYmJpeC5zaAo=" \
        --SecurityGroupIds '["sg-mdghqhdi"]' \
        --LoginSettings '{"KeyIds":["skey-4x41i3hz"]}' --RegionId ap-chongqing >./ace-node-return
        if ! jq .Response.InstanceIdSet ./ace-node-return >/dev/null 2>&1
        then
            tell "创建 $i 机器失败"
            rm_node
            exit 1
        fi
        InstanceId=$(jq .Response.InstanceIdSet ./ace-node-return | sed -e '1d' -e '$d' -e 's/"//g' -e 's/^ *//g')
        write_node_info ${InstanceId} $i
	sleep 20
    done
}


package_dir=/mnt/package/ace/ace_pack_$(date +%Y%m%d)
root_dir=/root/wanghongtao
cd ${root_dir}

if [ "${package_file}z" == "nonez" ]
then
    tell 'ACE 准备打包'
    rm -rf /mnt/package/ace/ace_pack*
    rm -rf /mnt/package/ace/newprivatedeploy
    cd /mnt/package/ace
    if [ "${branch}z" == "nonez" ]
    then
        tell "克隆newprivatedeploy 的master 分支"
        if ! git clone https://bitbucket.org/mathildetech/newprivatedeploy.git --depth=1
       # if !   ssh -i ~/.ssh/s root@119.28.23.245 -p 2222 "sh /ace/git.sh"
        then
            tell "克隆newprivatedeploy 的master 分支失败，退出"
            exit 1
        fi
    else
        tell "克隆newprivatedeploy 的${branch} 分支"
        if ! git clone -b ${branch} https://bitbucket.org/mathildetech/newprivatedeploy.git
        then
            tell "克隆newprivatedeploy 的${branch} 分支失败，开始克隆 master 分支"
            if ! git clone https://bitbucket.org/mathildetech/newprivatedeploy.git
            then
                tell "克隆newprivatedeploy 的master 分支失败，退出"
                exit 1
            fi
        fi
    fi
 #  scp -r -i ~/.ssh/s  -P 2222 root@119.28.23.245:/tmp/newprivatedeploy /mnt/package/ace
    mv /mnt/package/ace/newprivatedeploy ${package_dir}
    tell '更新 Chart.yaml 中，version 和 update 时间，并 push 到代码仓库中'
    sed -i -e "s/\(^version:.*\)\.[0-9]*$/\1\.$(($(cat ${package_dir}/chart/ACE/Chart.yaml | grep '^version:' | sed 's/^.*\.\([0-9]*$\)/\1/g')+1))/" -e "s/^\(description:.*\) Updated is .*$/\1 Updated is $(date +%Y-%m-%d_%T)/" ${package_dir}/chart/ACE/Chart.yaml
    tell "改动如下：$(cat ${package_dir}/chart/ACE/Chart.yaml | grep '^version:') $(cat ${package_dir}/chart/ACE/Chart.yaml | grep '^description:')"
   # tell "改动如下：$(ssh -i ~/.ssh/s root@119.28.23.245 -p 2222 "sh /ace/push.sh"|head -n1)"
    ssh -i ~/.ssh/s root@119.28.23.245 -p 2222 "sh /ace/rm.sh"
    cd ${package_dir}
    git add chart/ACE/Chart.yaml
    git commit -m "Script automatically updates version to $(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/^appVersion/{print $2}')-$(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/^version:/{print $NF}'), and update time is $(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/^description:/{print $NF}')"
    git push
    tell '拷贝ACP'
    cd /mnt/package/ace
    while :
    do
        if rsync -qa ACP ${package_dir}
        then
            break
        fi
    done
    tell '尝试启动镜像仓库和 chart repo'
    docker rm -f pkg-registry chart-repo
    #docker rm -f $(docker ps -qa)
    #tell '启动镜像仓库'
    if docker run -d --restart=always --name pkg-registry -p 60080:5000 -v ${package_dir}/ACP/registry:/var/lib/registry index.alauda.cn/alaudaorg/distribution:latest
    then
        tell '启动镜像仓库成功'
    else
        tell '启动镜像仓库失败'
        exit 1
    fi
    
    #tell '启动 chart repo'
    if docker run -d --restart=always --name chart-repo -e PORT=8080 -e DEBUG=1 -e STORAGE="local" -e STORAGE_LOCAL_ROOTDIR="/data" -e BASIC_AUTH_USER="chartmuseum" -e BASIC_AUTH_PASS="chartmuseum" -p 8088:8080 -v ${package_dir}/ACP/chartmuseum:/data 127.0.0.1:60080/chartmuseum:latest
    then
        tell '启动 chart repo 成功'
    else
        tell '启动 chart repo 失败'
        exit 1
    fi
    
    cd ${package_dir}
    rm -rf ${package_dir}/chart/ACE/charts/*
    #tell '构建 ACE 的 chart 的依赖'
    if helm dep build ${package_dir}/chart/ACE/
    then
        tell '构建 ACE 的 chart 的依赖成功'
    else
        tell '构建 ACE 的 chart 的依赖失败'
        exit 1
    fi
    #tell '创建 ACE chart'
    if helm package ${package_dir}/chart/ACE/
    then
        tell '创建 ACE chart 成功'
    else
        tell '创建 ACE chart 失败 '
        exit 1
    fi
    tell "ACE 的版本是 $(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/appVersion:/{print $2}' | sed 's/\"//g')-$(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/version:/{print $2}')"

    #tell '创建 e2e chart'
    if helm package ${package_dir}/chart/e2e-test/
    then
        tell '创建 e2e-test chart 成功'
    else
        tell '创建 e2e-test chart 失败 '
        exit 1
    fi
    
    #tell '上传 ACE 的 chart 到 chart repo'
    if curl -u chartmuseum:chartmuseum --data-binary @$(ls ${package_dir}/ACE-*.tgz) http://127.0.0.1:8088/api/charts
    then
        tell '上传 ACE 的 chart 到 chart repo 成功'
    else
        tell '上传 ACE 的 chart 到 chart repo 失败'
        exit 1
    fi

    #tell '上传 e2e 的 chart 到 chart repo'
    if curl -u chartmuseum:chartmuseum --data-binary @$(ls ${package_dir}/e2e-*.tgz) http://127.0.0.1:8088/api/charts
    then
        tell '上传 e2e 的 chart 到 chart repo 成功'
    else
        tell '上传 e2e 的 chart 到 chart repo 失败'
        exit 1
    fi

    tell 'clone 所有 global 组件的代码'
        #ssh -i ~/.ssh/s root@119.28.23.245 -p 2222 "sh /ace/clone.sh"
        # scp -P 2222 root@119.28.23.245:/tmp/code.tar /tmp
        # tar -xvf /tmp/code.tar
        # cp -r /tmp/code  ${package_dir}
#     ssh -i ~/.ssh/s root@119.28.23.245 -p 2222 "sh /ace/clone.sh" 
 #    scp -r -i ~/.ssh/s  -P 2222 root@119.28.23.245:/tmp/code ${package_dir}
  #   ssh -i ~/.ssh/s root@119.28.23.245 -p 2222 "sh /ace/clonerm.sh"
   # cd ${package_dir}
   # mkdir code
   # cd code
  #  for i in ${package_dir}/chart/ACE/templates/*_deploy.yaml
  #  do
   #     i=${i##*/}
    #    tell "clone ${i/_deploy.yaml/} 组件的代码"
   #     git clone https://hongtaowang@bitbucket.org/mathildetech/${i/_deploy.yaml/}.git
   #     rm -rf ${i/_deploy.yaml/}/.git*
   # done
    
    tell "生成镜像列表，存储在 ${package_dir}/image.list 文件中"
    helm template ${package_dir}/chart/ACE/ | awk '/index.alauda.cn/{print $NF}' | sed 's#index.alauda.cn/##g' >${package_dir}/image.list
    helm template ${package_dir}/chart/e2e-test/ | awk '/index.alauda.cn\//{print $NF}' | sed 's#index.alauda.cn/##g' >>${package_dir}/image.list
    cat ${package_dir}/*_images_list.txt | grep -v '^#' >>${package_dir}/image.list
    tell 'push 镜像到镜像仓库'
    for i in $(cat ${package_dir}/image.list)
    do
	sleep 2
        if ! docker pull index.alauda.cn/$i
        then
	sleep 1
           tell "pull index.alauda.cn/$i 失败"
       if ! docker pull index.alauda.cn/$i
       then
       sleep 1
	     tell "pull index.alauda.cn/$i 失败"
		if ! docker pull index.alauda.cn/$i
       		then
		sleep 1
			if ! docker pull index.alauda.cn/$i
                	then
	     tell "pull index.alauda.cn/$i 失败"
			sleep 1
				if ! docker pull index.alauda.cn/$i
                       		 then
				sleep 1
	     tell "pull index.alauda.cn/$i 失败"
					if ! docker pull index.alauda.cn/$i
                                 	then
					sleep 1
					tell "pull index.alauda.cn/$i 失败"
					exit 1
					fi
				fi
			fi
		fi
	fi 
		
           # exit 1
        fi
        docker tag index.alauda.cn/$i 127.0.0.1:60080/$i
        docker push 127.0.0.1:60080/$i
    done
	docker tag index.alauda.cn/alaudak8s/ake:1.19.12 127.0.0.1:60080/alaudak8s/ake:1.11
	docker push 127.0.0.1:60080/alaudak8s/ake:1.11
    tell '拷贝ATS'
    cp -r  /mnt/package/ats ${package_dir}
    tell '所有镜像都成功的 push，删除镜像仓库和 chart repo 容器，开始打包'
    
    docker rm -f pkg-registry chart-repo
    package_file=/mnt/package/ace_package/ACE_$(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/appVersion:/{print $2}' | sed 's/\"//g')-$(cat ${package_dir}/chart/ACE/Chart.yaml | awk '/version:/{print $2}')_$(date +%Y%m%d).tar.gz
    if tar zcfP ${package_file} ${package_dir}
    then
        tell "打包成功，下载地址是scp alauda@ops_doc.alauda.cn:${package_file} ，md5 是$(md5sum ${package_file} | tee -a /mnt/package/ALAUDA-release/md5.txt | awk '{print $1}') 文件大小是 $(ls -la ${package_file} | awk '{print $5}')"
    else
        tell '打包失败'
        exit 1
    fi
else
    tell "指定安装包为${package_file}，直接部署 ace"
    if [ ! -f ${package_file} ]
    then
        tell "找不到安装包 ${package_file} ，退出"
        exit 1
    fi
fi

tell '准备部署 ACE，创建虚拟机'
cd ${root_dir}
if [ "${single}z" == "truez" ]
then
    rm -rf temp.json node_list ace-node-return
    i=init
    [ "${project}z" == "z" ] || i=${project}-init
    qcloudcli cvm RunInstances  --Placement '{"Zone":"ap-chongqing-1","ProjectId":'${project_id}'}' --ImageId img-oikl1tzv --InstanceType SA1.2XLARGE16\
        --SystemDisk '{"DiskType":"CLOUD_PREMIUM","DiskSize":100}'  \
        --DataDisks '[{"DiskType":"CLOUD_PREMIUM","DiskSize":50}]' \
        --VirtualPrivateCloud '{"VpcId":"vpc-c9el9wfx","SubnetId":"subnet-6ohk5y8a"}' \
        --InternetAccessible '{"InternetChargeType":"TRAFFIC_POSTPAID_BY_HOUR","InternetMaxBandwidthOut":100,"PublicIpAssigned":"TRUE"}' \
        --InstanceCount 1  \
        --HostName "$i" \
        --InstanceName "$i-$(date +%Y%m%d)" \
        --UserData "IyEvYmluL2Jhc2gKL3Vzci9iaW4vd2dldCAtUCAvdG1wIGh0dHA6Ly8xMTguMjQuMjMzLjI4OjgwODAvemFiYml4LnNoICYmIC9iaW4vYmFzaCAvdG1wL3phYmJpeC5zaAo=" \
        --SecurityGroupIds '["sg-mdghqhdi"]' \
        --LoginSettings '{"KeyIds":["skey-4x41i3hz"]}' --RegionId ap-chongqing >./ace-node-return
    if ! jq .Response.InstanceIdSet ./ace-node-return >/dev/null 2>&1
    then
        tell "创建 $i 机器失败"
        rm_node
        exit 1
    fi
    sleep 10
    InstanceId=$(jq .Response.InstanceIdSet ./ace-node-return | sed -e '1d' -e '$d' -e 's/"//g' -e 's/^ *//g')
    write_node_info ${InstanceId} $i
else
    if mk_node
    then
        tell "创建虚拟机成功，服务器信息如下：$(cat node_list)"
    fi
fi
tell '检查虚拟机是否可以登录'
cat node_list | awk '{print $3" "$2}' >/tmp/hosts
echo '127.0.0.1 localhost' >>/tmp/hosts
for i in $(cat node_list| awk '{print $4}')
do
    if ! ssh -i ~/.ssh/tx -o StrictHostKeyChecking=no ${node_user_name}@$i ls / >/dev/null 2>&1
    then
        tell "$i 无法登录，删除虚拟机"
        rm_node
        exit 1
    fi
    scp -i ~/.ssh/tx /tmp/hosts ${node_user_name}@$i:/etc
    #scp -i ~/.ssh/tx ${package_dir}/lvm.sh ${node_user_name}@$i:/root
    if ssh -i ~/.ssh/tx ${node_user_name}@$i cat /etc/*rel* | grep -q -i suse
    then
        scp -i ~/.ssh/tx *.rpm ${node_user_name}@$i:/root
        ssh -i ~/.ssh/tx ${node_user_name}@$i rpm -ihv /root/sshpass-1.05-9.1.x86_64.rpm
        ssh -i ~/.ssh/tx ${node_user_name}@$i rpm -ihv /root/oniguruma-5.9.5-3.el7.x86_64.rpm
        ssh -i ~/.ssh/tx ${node_user_name}@$i rpm -ihv /root/jq-1.5-1.el7.x86_64.rpm
        ssh -i ~/.ssh/tx ${node_user_name}@$i mkfs.btrfs -f /dev/vdb
    else
       ssh -i ~/.ssh/tx ${node_user_name}@$i yum install -y jq sshpass &
   #    ssh -i ~/.ssh/tx ${node_user_name}@$i bash /root/lvm.sh vdb &
      ssh -i ~/.ssh/tx ${node_user_name}@$i mkfs.xfs -n ftype=1 /dev/vdb
    fi
    ssh -i ~/.ssh/tx ${node_user_name}@$i mkdir /var/lib/docker
   ssh -i ~/.ssh/tx ${node_user_name}@$i mount /dev/vdb /var/lib/docker
    ssh -i ~/.ssh/tx ${node_user_name}@$i sysctl -w vm.max_map_count=262144 &
    ssh -i ~/.ssh/tx ${node_user_name}@$i systemctl disable firewalld.service &
    ssh -i ~/.ssh/tx ${node_user_name}@$i systemctl stop firewalld.service &
    ssh -i ~/.ssh/tx ${node_user_name}@$i setenforce 0 &
done

tell "上传安装包到$(cat node_list | awk '/init/ {print $4}')"
if scp -i ~/.ssh/tx ${package_file} ${node_user_name}@$(cat node_list | awk '/init/ {print $4}'):/tmp
then
    tell '安装包成功上传到部署服务器'
else
    tell '上传安装包失败'
    rm_node
    exit 1
fi
tell '检查 md5'
package_file=${package_file##*/}
#if ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') md5sum /tmp/${package_file} | grep -q $(cat /mnt/package/ALAUDA-release/md5.txt | grep "${package_file}" | awk '{print $1}' | tail -n 1)
#then
#    tell '安装包 md5 值一致，安装包完整,准备解压缩'
#else
#    tell '安装包 md5 值不一致，安装包损坏，无法继续'
#    rm_node
#    exit 1
#fi

if ssh -i ~/.ssh/tx ${node_user_name}@$(cat node_list | awk '/init/ {print $4}') tar zxvf /tmp/${package_file} -C /root
then
    tell '解压缩安装包成功'
else
    tell '解压缩安装包失败，无法继续'
    rm_node
    exit 1
fi


tell '开始部署 ace'
test_report_path=/tmp/$(date +%Y%m%d%H%M)report
test_result=1
login_user=alauda
login_password=alauda
api_url="http://$(cat node_list | awk '/m-1/ {print $4}'):32001"
gloabl_registry="$(cat node_list | awk '/init/ {print $3}'):60080"
if install_ace
then
    for i in $(ssh -i ~/.ssh/tx root@$(cat node_list | awk '/m-1/ {print $4}') kubectl get pod -n alauda-system | awk '/^jakiro-/ {print $1}')
    do
          ssh -i ~/.ssh/tx root@$(cat node_list | awk '/m-1/ {print $4}') kubectl delete pod -n alauda-system $i
    done
    sleep 1m
    tell '部署 ace 成功，开始自动测试'
    docker pull index.alauda.cn/alaudaorg/api-test:latest
    docker run -t -v ${test_report_path}:/app/report -e API_URL=${api_url} -e PASSWORD=alauda -e REGION_NAME=create-new-region -e REGISTRY_NAME=create-new-region -e ENV=private -e GLOBAL_REGISTRY=${gloabl_registry} -e CASE_TYPE="not jenkins" -e TESTCASES=./test_case index.alauda.cn/alaudaorg/api-test:latest
    test_result="$?"
else
    tell '部署 ace 失败'
#    rm_node
    exit 1
fi

sleep 1m

if [ $test_result -eq 0 ]
then
    tell "自动测试成功，安装包${package_file},测试报告:${test_report_path}"
else
    tell "自动测试失败, 测试报告:${test_report_path} @韩后超 "
    exit 1
fi

tell '删除虚拟机'
#rm_node

