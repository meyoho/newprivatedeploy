#!/bin/bash

command_exists()
{
    command -v "$@" > /dev/null 2>&1
}
if [ ! -f /tmp/function.sh ]
then
    echo 'not find function.sh '
    exit 1
fi
. /tmp/function.sh

for i in $@
do
    eval $i
done

check_type='server_check'

system_check()
{
    ## begin system check
    local_dates=$(date +%s)
    detection_os_version
    check_type='node_os_check'
    [ "${auto_check}z" == "z" ] || rm -rf /tmp/alauda_checkstatus*
    if ls -la /tmp/alauda_checkstatus* >/dev/null 2>&1 | grep -q -v success
    then
        log_print ${check_type} error "The detection file /tmp/alauda_checkstatus was found. The last test failed or is being tested. Please check. If you want to continue, you need to delete /tmp/alauda_checkstatus\* file." 
        exit 1
    fi
    echo ''>/tmp/alauda_checkstatus_processing
    processing_status=0
    
    ## check time
    if [ $(echo "$((${dates}-${local_dates}))" | sed 's/-//g') -le 1 ]
    then
        log_print ${check_type} success "The time difference does not exceed 1 seconds. Remote time = ${date_hms}"
    else
        if [ $(echo "$((${dates}-${local_dates}))" | sed 's/-//g') -le 2 ]
        then
            log_print ${check_type} info "The time difference does not exceed 2 seconds. Remote time = ${date_hms}"
        else
            log_print ${check_type} critical "The difference in time is more than 2 seconds. Remote time = ${date_hms}"
            processing_status=1
        fi
    fi
    
    ## check time zone
    if timedatectl 2>/dev/null | grep -E 'Time zone|Timezone' | grep -q '+0800'
    then
        log_print ${check_type} success 'time zone = GMT+8 '
    else
        log_print ${check_type} error "Not China time zone, time zone = $(timedatectl 2>/dev/null | grep 'Time zone')"
        processing_status=1
    fi
    
    ## 检查 hosts 文件中，是否有重复 ip
    if [ "$(cat /etc/hosts | awk '{print $1}' | grep -v ^# | grep -v ^$ | sort | uniq | wc -l)" == "$(cat /etc/hosts | awk '{print $1}' | grep -v ^# | grep -v ^$ | wc -l)" ]
    then
        log_print ${check_type} success '/etc/hosts No duplicate ip address'
    else
        log_print ${check_type} error '/etc/hosts Duplicate ip address'
        processing_status=1
    fi
    
    ## check swap
    if [ "$(swapon --show | sed 1d | wc -l)" == "0" ] 
    then
        log_print ${check_type} success 'no swap'
    else
        log_print ${check_type} error "have swap partition $(swapon --show | sed 1d |awk '{printf $1}') "
        processing_status=1
    fi
    
    ## check hostname
    if hostname | grep -q -E '^[A-Za-z][A-Za-z0-9!-]{1,23}[A-Za-z0-9]$'
    then
        log_print ${check_type} success "hostname=$(hostname) , Meet the requirements"
    else
        log_print ${check_type} critical "hostname=$(hostname) , not meet the requirements" 
        processing_status=1
    fi
    
    ## check /tmp directory permissions
    if ls -l / | awk '{if($NF=="tmp") {k=0;s=0;for(i=0;i<=8;i++ ){k+=((substr($1,i+2,1)~/[rwxst]/)*2^(8-i))}j=4;for(i=4;i<=10;i+=3){s+=((substr($1,i,1)~/[stST]/)*j);j/=2}if(k){printf("%0o%0o ",s,k)}}}' | grep -q '.777'
    then
        log_print ${check_type} success "/tmp directory permissions = 777"
    else
        log_print ${check_type} error "/tmp directory permissions != 777"
        processing_status=1
    fi
    
    
    ## check selinux
    if command_exists getenforce 
    then
        if getenforce | grep -q Disabled
        then
            log_print ${check_type} success 'selinux is disable' 
        else
            log_print ${check_type} error 'selinux not is disable'
            processing_status=1
        fi
    fi

    ## check command
    command_list="curl tar ip ssh timedatectl ntpdate jq"
    for i in $(echo ${command_list})
    do
        if command_exists $i
        then
            #log_print ${check_type} success "command $i exists"
            #不打印日志，因为太多了
            :
        else
            log_print ${check_type} critical "command $i not exists"
            processing_status=1
        fi
    done
    
    ## check docker
    if command_exists docker
    then
        log_print ${check_type} info "Docker already installed"
        if ! docker info >/dev/null 2>&1
        then
            log_print ${check_type} error 'Docker daemon not runing'
            log_print ${check_type} info 'Try to start docker'
            if systemctl restart docker >/dev/null 2>&1
            then
                log_print ${check_type} info 'start docker success'
            else
                log_print ${check_type} error 'start docker failure'
                start_docker_daemon='1' ; processing_status=1
            fi
        fi
        if [ "${start_docker_daemon}z" != "1z" ]
        then
            docker_version_client=$(docker version | grep -A 5 Client: | awk '/Version:/{print $2}')
            docker_version_server=$(docker version | grep -A 5 Server: | awk '/Version:/{print $2}')
            log_print ${check_type} info "docker client version is ${docker_version_client}, docker server version is ${docker_version_server}"
            if [ $(docker ps -qa | wc -l ) -gt 0 ]
            then
                if [ "${NOT_DEPLOY_K8S}z" == "truez" ]
                then
                    log_print ${check_type} info 'A container exists on the server'
                else
                    log_print ${check_type} error 'A container exists on the server, Please remove all existed containers.'
                    processing_status=1
                fi
            fi
        fi
    fi
    
    ## check localhost resolve
    if ping -c 1 localhost 2>/dev/null | awk '/PING localhost./{print $3}' | grep -q 127.0.0.1 
    then
        log_print ${check_type} success 'localhost resolve success'
    else
        log_print ${check_type} error "localhost resolve other ip"
        processing_status=1
    fi
    
    
    ## check ipv6
    if ip -6 a >/dev/null 2>&1 
    then
        log_print ${check_type} success 'system load ipv6'
    else
        log_print ${check_type} error "system not load ipv6"
        processing_status=1
    fi
    
    
    ## Check the network device
    if ip addr ls ${net_dev} >/dev/null 2>&1 
    then
        log_print ${check_type} success "Find network card ${net_dev}"
    else
        log_print ${check_type} error "Can't find network card ${net_dev}"
        processing_status=1
    fi
    
    ## check ip addr
    if [ $(ip addr ls ${net_dev} 2>/dev/null 2>/dev/null | grep -E 'inet [0-9]' | awk '{print $2}' | sed 's#/.*$##g' | wc -l) -ne 1 ] 
    then
        log_print ${check_type} error "There are multiple ip addresses on the network card ${net_dev}"
        processing_status=1
    else
        log_print ${check_type} success "network card ${net_dev} ip = $(ip addr ls ${net_dev} | awk '/inet /{print $2}')"
    fi
    
    ## Check the default route
    if ip route show | grep -q 'default\|0.0.0.0' 
    then
        log_print ${check_type} success "There is a default route"
    else
        log_print ${check_type} critical "No default route"
        processing_status=1
    fi
    
    
    case "$lsb_dist" in
        sles)
    ## check suse file system
            if [ $(df -Th ${DOCKER_PATH} | sed -n '2p'|awk '{print $2}') == "btrfs" ]
            then
                log_print ${check_type} success "${DOCKER_PATH} File system is btrfs"
            else
                log_print ${check_type} critical "${DOCKER_PATH} File system is $(df -Th ${DOCKER_PATH} | sed -n '2p'|awk '{print $2}'), not btrfs"
                processing_status=1
            fi
    ## rm firewall
            SuSEfirewall2 off
        ;;
    
        centos)
            if [ "$dist_version" == "7" ] 
            then
                log_print ${check_type} success "centos or redhat version is $dist_version"
            else
                log_print ${check_type} critical "centos or redhat version is $dist_version , not support"
                processing_status=1
            fi
    ## check firewalld
            if systemctl status firewalld.service >/dev/null 2>&1
            then
                log_print ${check_type} error "firewalld start"
                processing_status=1
            else
                log_print ${check_type} success "firewalld stop"
            fi
#            if docker version 2>/dev/null | grep -q -E '1.12.6|17.03'
#            then
## check centos direct-lvm
#                if ! (lvdisplay | grep 'LV Name' | grep -q thinpool && lvdisplay | grep 'VG Name' | grep -q docker)
#                then
#                    log_print ${check_type} critical "could not find it direct-lvm"
#                    processing_status=1
#                else
#                    log_print ${check_type} success "find it direct-lvm"
#                fi
#            fi


        ;;
    esac
    

    ## check node list for /etc/hosts
    for i in $(cat /tmp/server_ipaddr_check_list 2>/dev/null)
    do
        if cat /etc/hosts | awk '{print $1}' | grep -q "^$i"
        then
            #log_print ${check_type} success "node $i in file /etc/hosts" 
            #不打印日志，因为太多了
            :
        else
            log_print ${check_type} error "node $i not in file /etc/hosts"
            processing_status=1
        fi
     done

    ## 创建检查状态文件
    if [ "${processing_status}" == "1" ]
    then
        rm -rf /tmp/alauda_checkstatus*
        echo ''>/tmp/alauda_checkstatus_fail
        exit 1
    fi
    
}
system_check


