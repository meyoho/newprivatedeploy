#!/bin/bash
set -e
## 设置变量
ALAUDA_EE_TEMP=/tmp/alauda_ee ; rm -rf ${ALAUDA_EE_TEMP} ; mkdir ${ALAUDA_EE_TEMP}
PWD_LOCAL_RUN=$(pwd)
[[ $0 =~ "/" ]] && cd ${0%/*}
PWD_LOCAL=$(pwd)
CHART_TGZ_FILE=$(ls ${PWD_LOCAL}/ACE*.tgz | head -n 1)
DOCKER_PATH=/var/lib/docker
NET_DEV=eth0
ACP_PATH=ACP
SERVER_LIST_FILE=${PWD_LOCAL}/server_list.json
HAPROXY_FILE=haproxy.cfg
cp ${PWD_LOCAL}/server_check.sh ${ALAUDA_EE_TEMP} ; SERVER_CHECK_SH_FILE=${ALAUDA_EE_TEMP}/server_check.sh
cp ${PWD_LOCAL}/function.sh ${ALAUDA_EE_TEMP} ; FUNCTION_SH_FILE=${ALAUDA_EE_TEMP}/function.sh
check_type='init'
. ${PWD_LOCAL}/function.sh "$@"
get_local_ip
K8S_CONTROL_LB=${LOCAL_IP}
DOMAIN_NAME=${LOCAL_IP}
cd ${PWD_LOCAL}/${ACP_PATH} ; ACP_PWD_LOCAL=$(pwd) ; cd $PWD_LOCAL_RUN
ACP_INIT_SCRIPT=${ACP_PWD_LOCAL}/init.sh


server_check()
{

## 检查传递进来的参数是否合法
    check_node_env=0
    if [ "${NETWORK_CIDR}" == '10.96.0.0/16' ]
    then
        log_print ${check_type} error 'The cidr of the k8s cluster cannot use 10.96.0.0/16, default is 10.199.0.0/16' ; check_node_env=1
    fi
## 检查
    cp ${ALAUDA_EE_TEMP}/* /tmp/ >/dev/null 2>&1
    bash /tmp/${SERVER_CHECK_SH_FILE##*/} dates=$(date "+%s") date_hms=$(date "+%T") net_dev=${NET_DEV} DOCKER_PATH=${DOCKER_PATH} auto_check=true 2>/dev/null || check_node_env=1
    if [ "${check_node_env}" == "1" ]
    then
        log_print ${check_type} error 'Some server checks failed' ;  exit 1
    else
        log_print ${check_type} info 'All servers check through'
    fi
}

init_base()
{
## 调用 ACP 的 init.sh 脚本，初始化基本环境，部署镜像仓库、软件源、chart repo，将 ACE 的 chart 传到 chart repo 上
    . ${ACP_INIT_SCRIPT}
    sleep 5
    if curl -u $(echo ${CHART_ENDPOINT} | sed -e 's/@.*$//g' -e 's#^.*://##g') --data-binary "@${CHART_TGZ_FILE}" $(echo ${CHART_ENDPOINT} | sed 's#^http.*@#http://#')api/charts 2>/dev/null | jq . | grep -q -E '"saved": true|"file already exists"' 2>/dev/null 
    then
        log_print ${check_type} success "upload ACE chart to ${CHART_ENDPOINT} success"
    else
        log_print ${check_type} error "upload ACE chart to ${CHART_ENDPOINT} failed" ; exit 1
    fi
## 启动数据库
    log_print deploy_db info 'Deploy database'
    log_print deploy_db warning 'Used only for poc testing !!!!!!'
    cat <<EOF >/alauda/.run_db.sh
#!/bin/bash
docker run -d --restart=always --name mysql -p 3306:3306 -v /alauda/data/mysql/db:/var/lib/mysql -e MYSQL_ROOT_PASSWORD="sgOnxrI9WN638Hj8" ${REGISTRY_ENDPOINT}/alaudaorg/mysql-master:latest >/dev/null 2>&1
EOF
    DB_PORT='3306' ; DB_PASSWORD='sgOnxrI9WN638Hj8' ; DB_USER='root' ; DB_HOST="${LOCAL_IP}" ; DB_ENGINE='mysql'
    if bash /alauda/.run_db.sh
    then
        log_print deploy_db success 'Deploy sql database success'
        log_print deploy_db info "The database information: DB_PORT=$DB_PORT DB_PASSWORD=$DB_PASSWORD DB_USER=$DB_USER DB_HOST=$DB_HOST DB_ENGINE=$DB_ENGINE"
    else
        log_print deploy_db error 'Failed to start database'
        exit 1
    fi
## 检查数据库链接和写权限
    log_print check_db info 'Check the database'
    sleep 50
    /usr/bin/checkdb -e ${DB_ENGINE} -h ${DB_HOST} -k ${DB_PASSWORD} -p ${DB_PORT} -u ${DB_USER} >/tmp/check_db_info 2>&1 
    if grep -q 'Succese connect to database.' /tmp/check_db_info
    then
        log_print check_db success 'Successful access to the database'
    else
        log_print check_db error 'Access to the database failed'
        exit 1
    fi 
    if grep -q 'Success write to database.' /tmp/check_db_info
    then
        log_print check_db success "Write database successfully, user ${DB_USER} has the correct permissions"
    else
        log_print check_db error "Failed to write database, user ${DB_USER} does not have write permission"
        exit 1
    fi
## 创建数据库
    rm -rf /tmp/alaudaee_chart
    mkdir /tmp/alaudaee_chart
    tar zxf ${CHART_TGZ_FILE} -C /tmp/alaudaee_chart
    DB_NAME_LIST=($(find /tmp/alaudaee_chart -name '*.yaml' -exec grep -A 1 ' name: DB_NAME' {} \; | awk '/value:/ {print $2}'|tr -d "'" | sort))
    log_print create_db info "Start creating the following databases: ${DB_NAME_LIST[@]}"
    for i in ${DB_NAME_LIST[@]}
    do
        /usr/bin/checkdb -e ${DB_ENGINE} -h ${DB_HOST} -k ${DB_PASSWORD} -p ${DB_PORT} -u ${DB_USER} -d $i >/tmp/check_db_info 2>&1
        if grep -q -E "Success create database:  $i|pq: database \"$i\" already exists" /tmp/check_db_info
        then
            log_print create_db success "Create database $i successfully"
        else
            log_print create_db error "Create database $i failed"
            exit 1
        fi
    done

}

install_k8s()
{

## 部署 k8s 集群
    cat <<EOF >/alauda/.run_up_k8s.sh
#!/bin/bash
set -e
ake up \\
    --dockercfg="${DOCKER_CONFIG_FILE_TEMP}" \\
    --pkg-repo="${KALDR_ENDPOINT}" \\
    --registry="${REGISTRY_ENDPOINT}" \\
    --chart_stable_repo_addr="${CHART_ENDPOINT}" \\
    --alauda_chart_repo="${CHART_ENDPOINT}" \\
    --oidc-issuer-url="https://${K8S_CONTROL_LB}/dex" \\
    --acp-host=${K8S_CONTROL_LB} \\
    --network-opt="backend_type=${NETWORK_BACKEND}" \\
    --kube-pods-subnet="${NETWORK_CIDR}" \\
    --enabled-features=helm,alauda-ingress,alauda-devops,alauda-dashboard,alauda-portal,alauda-oidc,alauda-base,alauda-appcore \\
    --oidc_ca_file=/alauda/ssl/cert.pem \\
    --oidc_ca_key=/alauda/ssl/key.pem \\
EOF
## 创建证书
    log_print install_k8s info 'create /alauda/ssl/cert.pem and /alauda/ssl/key.pem'
    rm -rf /alauda/ssl
    mkdir -p /alauda/ssl
    cat << EOF > /alauda/ssl/req.cnf
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectAltName = @alt_names
[alt_names]
IP.1=${K8S_CONTROL_LB}
EOF

    openssl genrsa -out /alauda/ssl/ca-key.pem 2048
    openssl req -x509 -new -nodes -key /alauda/ssl/ca-key.pem -days 8000 -out /alauda/ssl/ca.pem -subj "/CN=kube-ca"
    if openssl genrsa -out /alauda/ssl/key.pem 2048
    then
        log_print install_k8s success 'create /alauda/ssl/key.pem success'
    else
        log_print install_k8s success 'create /alauda/ssl/key.pem failed' ; exit 1
    fi
    openssl req -new -key /alauda/ssl/key.pem -out /alauda/ssl/csr.pem -subj "/CN=kube-ca" -config /alauda/ssl/req.cnf
    if openssl x509 -req -in /alauda/ssl/csr.pem -CA /alauda/ssl/ca.pem -CAkey /alauda/ssl/ca-key.pem -CAcreateserial -out /alauda/ssl/cert.pem -days 8000 -extensions v3_req -extfile /alauda/ssl/req.cnf
    then
        log_print install_k8s success 'create /alauda/ssl/cert.pem success'
    else
        log_print install_k8s success 'create /alauda/ssl/cert.pem failed' ; exit 1
    fi

    [ "${K8S_CONTROL_LB}z" == "z" ] && sed -i '/--kube_controlplane_endpoint.*$/d' /alauda/.run_up_k8s.sh
    [ "${NODES}z" == 'z' ] && sed -i '/--nodes=.*$/d' /alauda/.run_up_k8s.sh
    [ "${ACP_SIMPLE}z" == 'truez' ] && sed -i '/--enabled-features=.*$/d' /alauda/.run_up_k8s.sh
    [ "${DEBUG}z" == "truez" ] && sed -i 's/^ake deploy/ake deploy --debug/' /alauda/.run_up_k8s.sh
    tailf -n 0 /alauda/alauda.log 2>/dev/null 2>/dev/null &
    if bash /alauda/.run_up_k8s.sh >> /alauda/alauda.log
    then
        kill -9 $(ps aux | grep 'tailf -n 0 /alauda/alauda.log' | grep -v grep | awk '{print $2}') >/dev/null 2>&1
        if tail -n 5 /alauda/alauda.log | grep -q '❌'
        then
            log_print install_k8s error 'Failed to deploy k8s cluster' ; exit 1
        else
            log_print install_k8s info 'Successful deployment of k8s cluster'
        fi
    else
        kill -9 $(ps aux | grep 'tailf -n 0 /alauda/alauda.log' | grep -v grep | awk '{print $2}') >/dev/null 2>&1
        log_print install_k8s error 'Failed to deploy k8s cluster'
        exit 1
    fi
    if cat /sys/fs/cgroup/memory/kubepods/memory.kmem.slabinfo 2>&1 | grep -Eq '输入/输出错误|Input/output error'
    then
        log_print up_alauda_ee success 'k8s version is correct, it is hexiaoxi modified'
    else
        log_print up_alauda_ee error 'K8s version error, not hexiaoxi modified'
    fi

}

check_k8s()
{

## 检查 k8s 集群是否正常
    cat <<EOF >/tmp/check_k8s_cluster.sh
#!/bin/bash
kubernets_cluster_status=0
\$(whereis -b kubectl | awk '{print \$2}') get pod --all-namespaces -o wide | sed 1d >/tmp/kubectl_get_pod.txt
j=1 ; >/tmp/not_ready_pod_list.txt
for i in \$(cat /tmp/kubectl_get_pod.txt | awk '{print \$3}')
do
    pod_ready=(\${i//// })
    [ "\${pod_ready[0]}" != "\${pod_ready[1]}" ] && echo \$j >>/tmp/not_ready_pod_list.txt
    ((j++))
done
if [ -s /tmp/not_ready_pod_list.txt ]
then
    sed_parameter='' ; kubernets_cluster_status=1
    for i in \$(cat /tmp/not_ready_pod_list.txt)
    do
        sed_parameter="\${sed_parameter}p;\$i"
    done
    echo -e "\033[1;34m[check_k8s     ]\033[1;31m[ERROR   ]\033[0m[\$(date '+%Y%m%d-%T')][\$(hostname -i | head -n 1)]\t\033[1;31m[The following pods are not working properly]\033[0m"
    cat /tmp/kubectl_get_pod.txt | sed -n "\${sed_parameter/p;/}p"
fi
\$(whereis -b kubectl | awk '{print \$2}') get no | sed 1d >/tmp/kubectl_get_no.txt
j=1 ; >/tmp/not_ready_node_list.txt
for i in \$(cat /tmp/kubectl_get_no.txt | awk '{print \$2}')
do
    [ \$i == 'Ready' ] || echo \$j >>/tmp/not_ready_node_list.txt
    ((j++))
done
if [ -s /tmp/not_ready_node_list.txt ]
then
    sed_parameter='' ; kubernets_cluster_status=1
    for i in \$(cat /tmp/not_ready_node_list.txt)
    do
        sed_parameter="\${sed_parameter}p;\$i"
    done
    echo -e "\033[1;34m[check_k8s     ]\033[1;31m[ERROR   ]\033[0m[\$(date '+%Y%m%d-%T')][\$(hostname -i | head -n 1)]\t\033[1;31m[The following nodes are not working properly]\033[0m"
    cat /tmp/kubectl_get_no.txt | sed -n "\${sed_parameter/p;/}p"
fi
if [ \${kubernets_cluster_status} == 1 ]
then
    echo -e "\033[1;34m[check_k8s     ]\033[1;31m[ERROR   ]\033[0m[\$(date '+%Y%m%d-%T')][\$(hostname -i | head -n 1)]\t\033[1;31m[The k8s cluster found an error and cannot continue]\033[0m"
    exit 1
fi
EOF
## 检查集群
    cp /tmp/check_k8s_cluster.sh /tmp/ >/dev/null 2>&1
    for i in `seq 1 6`
    do
        log_print check_k8s info 'info Wait 20 seconds' ; sleep 20
        if bash /tmp/check_k8s_cluster.sh 2>/dev/null
        then
            log_print check_k8s success "In the k8s cluster, all pod and node states are ready."
            break
        else
            log_print check_k8s error "The k8s cluster status is abnormal"
            if [ $i -gt 6 ]
            then
                log_print check_k8s error "After three checks, the node and pod status of the k8s cluster is still not all normal. Please check" ; 
            fi
        fi
    done
## 获取 node 的 json 文件
    log_print check_k8s info 'execute "kubectl get node -o json"'
    if kubectl get node -o json >/tmp/kubectl_get_node.json
    then
        if ! jq . /tmp/kubectl_get_node.json >/dev/null 2>&1
        then
            log_print check_k8s error "Failed to execute kubectl get node -o json >/tmp/kubectl_get_node.json"
            exit 1
        fi
    else
        log_print check_k8s error "Failed to execute kubectl get node -o json >/tmp/kubectl_get_node.json"
        exit 1
    fi

    if [ "${check_docker_registry}" == "1" ]
    then
        log_print check_k8s error 'Some server docker configuration errors' ; exit 1
    fi

}

config_k8s()
{
## 配置 k8s 集群，给 node 加 label
## label 从/tmp/node_lalel_${IP_ADDR}.list 文件中获取
## k8s 集群的 node 的信息，从/tmp/kubectl_get_node.json 文件中获取
    cat <<EOF > /tmp/set_node_label.sh
#!/bin/bash
set -e
kubectl label nodes $(hostname) ip=${LOCAL_IP} --overwrite
EOF
    for i in global redis log
    do
        echo "kubectl label nodes $(hostname) ${i}=true --overwrite" >>/tmp/set_node_label.sh
    done
    if bash /tmp/set_node_label.sh >/dev/null 2>&1
    then
        log_print config_k8s success "Successful execution of /tmp/set_node_label.sh script, The commands executed are as follows: bash /tmp/set_node_label.sh 2>/dev/null'"
    else
        log_print config_k8s error "Failed to execute /tmp/set_node_label.sh script,The commands executed are as follows: bash /tmp/set_node_label.sh 2>/dev/null'"
        exit 1
    fi

}

up_alauda_ee()
{
## 通过 chart 启动 ACE
## 包括 pv、pvc、configmap、namespaces、limitranges、deploy
## 创建 namespaces
    log_print up_alauda_ee info 'create ACE namespaces'
    if kubectl get namespaces | grep -q "^alauda-system" 2>/dev/null
    then
        log_print up_alauda_ee info 'namespaces alauda-system already exists'
    else
        if kubectl create namespace alauda-system >/dev/null 2>/dev/null
        then
            log_print up_alauda_ee success "create ACE namespaces successfully"
        else
            log_print up_alauda_ee error "create ACE namespaces failed, The commands executed are as follows: kubectl create namespace alauda-system 2>/dev/null"
            exit 1
        fi
    fi
## 通过 helm 安装 ACE
    log_print up_alauda_ee info 'Install ACE via helm'
    sleep 30

## 生成密码
    es_pass=$(</dev/urandom tr -dc A-Z-a-z | head -c10)
    es_pass_64=$(echo ${es_pass} | base64)
    es_user_64=$(echo alaudaes | base64)
    redis_pass=$(</dev/urandom tr -dc A-Z-a-z | head -c10)
    redis_pass_64=$(echo ${redis_pass} | base64)
    DB_USER_64=$(echo ${DB_USER} | base64)
    DB_PASSWORD_64=$(echo ${DB_PASSWORD} | base64)

## 生成redis.toml 文件
    cat <<EOF >/tmp/redis.toml
# db 密码
DB_PASSWORD="${DB_PASSWORD}"

# jakrio 密码
JAKIRO_PASSWORD='TVFcc]J%p35x/Z>'

# normal为单例，cluster为集群
REDIS_TYPE_READER="normal"

# redis的DB name，只有在单例模式下才管用，集群模式会被忽略
REDIS_DB_NAME_READER=0

# 密码,如果没有密码则传空
REDIS_DB_PASSWORD_READER="${redis_pass}"
REDIS_DB_PASSWORD_WRITER="${redis_pass}"

# 最大连接数, 默认为32
REDIS_MAX_CONNECTIONS_READER=32

# redis key的前缀
REDIS_KEY_PREFIX_READER="alauda"

# 当用户屏蔽了CONFIG命令时,需要把这个值改为true，只有集群模式有这个变量
REDIS_SKIP_FULL_COVER_CHECK_READER=false

# redis连接和操作的超时时间为5秒
REDIS_TIMEOUT_READER=5

# ip，集群模式下IP会有多个，以逗号分割，IP数量和端口数量一致
REDIS_HOST_READER=["${LOCAL_IP}"]

# 端口，集群模式下端口会有多个，以逗号分割，IP数量和端口数量一致
REDIS_PORT_READER=["26379"]
EOF

    echo 'helm install --set global.variable.redis_node_num="'1'" --set global.variable.redis_node1='${LOCAL_IP}' --set global.singlenode="true" --set global.kafka.ha_switch="false" --set global.variable.redis_toml="'$(cat /tmp/redis.toml | base64 -w 0)'" --set global.db_user_64="'${DB_USER_64}'" --set global.db_passwd_64="'${DB_PASSWORD_64}'" --set global.es_user_64="'${es_user_64}'" --set global.es_passwd_64="'${es_pass_64}'" --set global.es_passwd="'${es_pass}'" --set global.variable.redis_passwd="'${redis_pass}'" --set global.variable.redis_passwd_base64="'${redis_pass_64}'" --set global.variable.vip='${DOMAIN_NAME}' --set global.variable.db_engine='${DB_ENGINE}' --set global.variable.db_host='${DB_HOST}' --set global.variable.db_port='${DB_PORT}' --set global.variable.db_user='${DB_USER}' --set global.variable.db_password='${DB_PASSWORD}' --set global.variable.chart_repo_url='${CHART_ENDPOINT}' --set global.variable.regrstry_ep='${REGISTRY_ENDPOINT}' --set global.registry='${REGISTRY_ENDPOINT}' --set replicas="'1'" --namespace '$(grep 'namespace:' /tmp/alaudaee_chart/ACE/values.yaml | awk '{print $2}') ' --name alauda-ace --debug stable/ACE' >/tmp/helm_install.sh
    if bash /tmp/helm_install.sh  >/dev/null 2>&1
    then
        log_print up_alauda_ee info 'Helm executed successfully, waiting for ACE to start successfully'
        log_print up_alauda_ee info 'Check every minutes, check 10 times in total, if ACE still does not start successfully, please check'
	sleep 10
        log_print up_alauda_ee info 'rebuild all pod'
	kubectl delete po -n alauda-system --all >/dev/null 2>&1
        sleep 20
        for i in {1..11}
        do
            sleep 60
            if [ $i == 11 ] ; then log_print up_alauda_ee error 'ACE did not start successfully' ; exit 1 ; fi
            kubectl get pod -n alauda-system -o wide |grep -E "architect|chen2|darchrow|davion|enigma|heimdall|jakiro|jakiro2|kaldr|krobelus|lanaya|lightkeeper2|lucifer|morgans|razzil|rubick|tiny|tiny-proxy|wisp|kunkka|furion|hydra|hedwig|redis|elasticsearch" >/tmp/alaudaee_pod_list 2>/dev/null
            cat /tmp/alaudaee_pod_list | awk -F '[ /]+' '(NR>1 && $2!=$3){print}' >/tmp/alauda_pod_status
            if [ -s /tmp/alauda_pod_status ]
            then
                log_print up_alauda_ee warning 'The following pods did not start successfully'
                cat /tmp/alauda_pod_status
            else
                log_print up_alauda_ee success 'All pods started successfully'
                break
            fi
        done
    else
        log_print up_alauda_ee error "Helm executed failed, The commands executed are as follows: $(cat /tmp/helm_install.sh) "
        exit 1
    fi
## 检查 pod 是否全部启动，利用以前的脚本，所有的 pod 都检查，同时检查 node
    for i in {1..12}
    do
        log_print check_alauda info 'info Wait 60 seconds' ; sleep 60
        if bash /tmp/check_k8s_cluster.sh 2>/dev/null
        then
            log_print check_k8s success "all pod and node states are ready."
            break
        else
            log_print check_k8s error "status is abnormal"
            if [ $i -gt 10 ]
            then
                log_print check_k8s error "After ten checks, the pod status of the ACE is still not all normal. Please check" ; exit 1
            fi
        fi
    done


## migrate
    log_print up_alauda_ee info "Execute the /tmp/migrate.sh script, Command is as follows: bash /tmp/migrate.sh"
    cp ${PWD_LOCAL}/migrate.sh /tmp/ >/dev/null 2>&1
    if bash /tmp/migrate.sh
    then
        log_print up_alauda_ee info 'migrate success'
    else
        log_print up_alauda_ee error 'migrate error, deployment failed'
        exit 1
    fi
    log_print up_alauda_ee warning 'After the script is executed, it is not sure that all components migrate are all successful'
}

add_something()
{
    log_print delpoy_alauda info 'sleep 60s, Waiting for redis cluster ready'
    sleep 60
    superuser_pass=$(</dev/urandom tr -dc A-Z-a-z | head -c10)
    superuser_name=alauda
    
## 创建根用户，添加 global 集群之类的操作
    log_print delpoy_alauda info 'create admin user'
    if curl -X POST \
         -H 'Content-Type:application/json' \
         -d '{
    "username":"'${superuser_name}'",
    "password":"'${superuser_pass}'",
    "realname":"alauda",
    "email":"alauda@alauda.io",
    "reference_code_id":"cn",
    "city":"beijing",
    "company":"alauda",
    "is_active": true,
    "informed_way":"others",
    "industry":"others",
    "mobile":"123123123123",
    "position":"beijing",
    "currency":"CNY"
}' http://${LOCAL_IP}:$(cat /tmp/alaudaee_chart/ACE/values.yaml | grep -A1 '^  jakiro:' | awk '/node_port_80/ {print $2}')/v1/auth/register/ >/dev/null 2>&1
    then
        log_print delpoy_alauda success "create admin user success. username:${superuser_name}   password:${superuser_pass}"
    else
        log_print delpoy_alauda error 'create admin user failure'
        exit 1
    fi
    sleep 3
## 获取k8s 集群的 token ，接入 global 集群
    global_token=''
    cluster_secret_name=$(kubectl get secret -n kube-system | awk '/^clusterrole-aggregation-controller-token/{print $1}')
    log_print delpoy_alauda info 'Get the token of the global cluster'
    if global_token=$(kubectl get secret -n kube-system ${cluster_secret_name} -o jsonpath='{.data.token}' | base64 -d)
    then
        log_print delpoy_alauda success 'Get the token of the global cluster success'
    else
        log_print delpoy_alauda error 'Get the token of the global cluster failure'
        exit 1
    fi
## 获取根用户的 token
    ace_user_token=''
    log_print delpoy_alauda info 'get alauda token'
    if ace_user_token=$(curl -fsSL -X POST \
      http://${LOCAL_IP}:$(cat /tmp/alaudaee_chart/ACE/values.yaml | grep -A1 '^  jakiro:' | awk '/node_port_80/ {print $2}')/v1/generate-api-token \
      -H 'content-type: application/json' \
      -d '{
        "email": "alauda@alauda.io",
        "username": "'${superuser_name}'",
        "password": "'${superuser_pass}'"
    }' | awk '{print $4}' | sed -e 's/"//g' -e 's/,//g')
    then
        log_print delpoy_alauda success "get ${superuser_name} token success"
    else
        log_print delpoy_alauda error "get ${superuser_name} token failure"
        exit 1
    fi
## 接入 global集群
    region_docker_version=$(docker version --format '{{.Server.Version}}')
    region_k8s_version=$(kubectl version --output=json | jq .serverVersion.gitVersion | sed 's/[^0-9.]//g')
    log_print delpoy_alauda info "add global region, docker=${region_docker_version} k8s=${region_k8s_version}"
    cat <<EOF >/tmp/add_region.json
{
  "display_name": "ACE",
  "name": "ace",
  "namespace": "${superuser_name}",
  "attr": {
    "cluster": {
      "nic": "${NET_DEV}"
    },
    "docker": {
      "path": "/var/lib/docker",
      "version": "${region_docker_version}"
    },
    "cloud": {
      "name": "PRIVATE"
    },
    "kubernetes": {
      "type": "original",
      "endpoint": "https://${LOCAL_IP}:6443",
      "token": "${global_token}",
      "version": "${region_k8s_version}",
      "cni": {
        "type": "flannel",
        "cidr": "10.199.0.0/16",
        "backend": "vxlan"
      }
    }
  },
  "features": {
  }
}
EOF
    if curl -X POST \
         -H "Authorization: Token ${ace_user_token}" \
         -H 'Content-Type:application/json' \
         --data @/tmp/add_region.json \
         http://${LOCAL_IP}:$(cat /tmp/alaudaee_chart/ACE/values.yaml | grep -A1 '^  jakiro:' | awk '/node_port_80/ {print $2}')/v2/regions/${superuser_name} >/dev/null 2>&1
    then
        log_print delpoy_alauda success 'Add global cluster success'
    else
        log_print delpoy_alauda error 'Add global cluster failure'
        exit 1
    fi

}


## check command
for i in sshpass jq
do
    if ! command_exists $i
    then
        log_print check_command error "$i command does not exist"
        exit 1
    fi
done

cp ${PWD_LOCAL}/checkdb /usr/bin
chmod a+x /usr/bin/checkdb
server_check
do_it init_base
do_it install_k8s
do_it check_k8s
do_it config_k8s
do_it up_alauda_ee
do_it add_something
